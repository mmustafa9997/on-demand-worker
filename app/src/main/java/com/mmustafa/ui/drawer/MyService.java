package com.mmustafa.ui.drawer;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.mmustafa.ui.drawer.utils.RequestUrlForNavigationDrawerActivity;

import java.util.Timer;
import java.util.TimerTask;

public class MyService extends Service {

    public int notify = 3000;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    Double mLat, mLng;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        if (mTimer != null)
            mTimer.cancel();
        else
            mTimer = new Timer();

        initLocationService();

        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();

        stopLocationUpdates();
        //Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }

    public void initLocationService() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        createLocationCallback();
        createLocationRequest();
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                updateLocationUI();
            }
        };
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            mLat = mCurrentLocation.getLatitude();
            mLng = mCurrentLocation.getLongitude();
            RequestUrlForNavigationDrawerActivity.updateWorkerLocation(this, mLat, mLng);
            //Toast.makeText(this, "Phone : "+ ConfigURL.getMobileNumber(this) +"Lat: " + mCurrentLocation.getLatitude() + "Lng: " + mCurrentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
        }
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, null);
        updateUI();

    }

    private void updateUI() {
        updateLocationUI();
    }

    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    startLocationUpdates();
                    // Toast.makeText(getApplicationContext(), "Service Running", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}