package com.mmustafa.ui.drawer.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Qasim Ahmed on 13/11/2017.
 */

public class ConfigURL {
    public static final String URL_REGISTRATION_IS_WORKER_EXIST = "http://www.itehadmotors.com/LabourOnDemand/v1/isPersonExist";

    public static final String URL_REGISTER_WORKER = "http://www.itehadmotors.com/LabourOnDemand/v1/worker";

    public static final String URL_LOGIN_WORKER = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/login/worker";

    public static final String URL_FORGOT_WORKER_PASS = "http://www.itehadmotors.com/LabourOnDemand/v1/forgot";

    public static final String URL_FORGOT_WORKER_PASS_MOBILE_CHECK = "http://www.itehadmotors.com/LabourOnDemand/v1/isRegistered";

    public static final String URL_GET_PERSON_PROFILE = "http://www.itehadmotors.com/LabourOnDemand/v1/workerProfile/";

    public static final String URL_GET_WORKER_ORDERS = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/workerJobs/";

    public static final String URL_REGISTRATION = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/worker";

    public static final String URL_PROFILE_CHANGE_PASSWORD = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/worker/changePassword";

    public static final String URL_GET_WALLET_AMOUNT = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/worker/wallet/";

    public static final String URL_POST_JOB_CANCEL = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/worker/jobCancel";

    public static final String URL_GET_CATEGORY_FOR_REGISTRATION = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/category";

    public static final String URL_UPDATE_WORKER_LOCATION = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/updateWorkerLocation";

    public static final String URL_SEND_TOKEN = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/fcm";

    public static final String URL_JOB_ACCEPTANCE = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/jobaccept";

    public static final String URL_JOB_WORKER_ARRIVED = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/updatejobstatustoarrive";

    public static final String URL_JOB_WORKER_START = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/updatejobstatustostart";

    public static final String URL_JOB_WORKER_FINISH = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/updatejobstatustocomplete";

    public static final String URL_CHECK_JOB_STATUS = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/workerJobstatus";

    public static String URL_GET_WORKER_IMAGE = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/workerImage";

    public static String URL_GET_JOB_TIME = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/timetaken";

    public static String URL_GET_WORKER_TRANSACTION = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/worker/transaction/";

    public static String URL_ACCEPTABLE_AMOUNT = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/jobamountforcustomer";

    public static String getLoginState(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("LOGIN", "").length() > 0) {
            return prefs.getString("LOGIN", "");
        } else
            return "";
    }

    public static String getMobileNumber(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("PHONE", "").length() > 0) {
            return prefs.getString("PHONE", "");
        } else
            return "";
    }

    public static String getWorkerName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("NAME", "").length() > 0) {
            return prefs.getString("NAME", "");
        } else
            return "";
    }

    public static String getWorkerEmail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("EMAIL", "").length() > 0) {
            return prefs.getString("EMAIL", "");
        } else
            return "";
    }

    public static String getJobAddress(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("ADDRESS", "").length() > 0) {
            return prefs.getString("ADDRESS", "");
        } else
            return "";
    }

    public static String getJobTitle(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("TITLE", "").length() > 0) {
            return prefs.getString("TITLE", "");
        } else
            return "";
    }

    public static String getJobLat(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("LAT", "").length() > 0) {
            return prefs.getString("LAT", "");
        } else
            return "";
    }

    public static String getJobLng(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("LNG", "").length() > 0) {
            return prefs.getString("LNG", "");
        } else
            return "";
    }

    public static String getWorkerProfilePic(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("PROFILEPIC", "").length() > 0) {
            return prefs.getString("PROFILEPIC", "");
        } else
            return "";
    }

    public static String getJobMobileNumber(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("CUSTOMERMOBILE", "").length() > 0) {
            return prefs.getString("CUSTOMERMOBILE", "");
        } else
            return "";
    }

    public static String getJobID(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("JOBID", "").length() > 0) {
            return prefs.getString("JOBID", "");
        } else
            return "";
    }

    public static String getOnStatus(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("ONSTATUS", "").length() > 0) {
            return prefs.getString("ONSTATUS", "");
        } else
            return "";
    }

    public static String getClickableState(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("CLICKABLE", "").length() > 0) {
            return prefs.getString("CLICKABLE", "");
        } else
            return "";
    }

    public static void clearshareprefrence(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}