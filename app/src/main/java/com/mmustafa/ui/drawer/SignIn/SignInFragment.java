package com.mmustafa.ui.drawer.SignIn;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.DrawerMainActivity;
import com.mmustafa.ui.drawer.Models.Worker;
import com.mmustafa.ui.drawer.Notification.SendRegistrationTokenFCM;
import com.mmustafa.ui.drawer.R;
import com.mmustafa.ui.drawer.SignUp.SignUpInfoFragment;
import com.mmustafa.ui.drawer.utils.NetworkConnectivityClass;
import com.mmustafa.ui.drawer.utils.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment implements View.OnClickListener {

    TextView tv_new_account, tv_forgot_pass;
    EditText et_input_phone, et_input_password;
    String phone, pass;
    Button btn_signin;
    String LOGIN = "active";

    public SignInFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);


        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();


        if (ConfigURL.getLoginState(getActivity()).length() > 0) {
            Intent intent = new Intent(getActivity(), DrawerMainActivity.class);
            startActivity(intent);
            getActivity().finish();
        }


        et_input_phone = (EditText) rootView.findViewById(R.id.et_input_phone);
        et_input_password = (EditText) rootView.findViewById(R.id.et_input_password);

        tv_new_account = (TextView) rootView.findViewById(R.id.tv_new_account);
        tv_forgot_pass = (TextView) rootView.findViewById(R.id.tv_forgot_pass);
        btn_signin = (Button) rootView.findViewById(R.id.btn_signin);

        tv_new_account.setOnClickListener(this);
        tv_forgot_pass.setOnClickListener(this);
        btn_signin.setOnClickListener(this);

        return rootView;
    }

    public void submit() {
        if (!validatePhone()) {
            return;
        }
        if (et_input_password.getText().toString().isEmpty()) {
            et_input_password.setError("Password Cannot Be Empty");
            requestFocus(et_input_password);
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(getActivity())) {
            sendData();
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), "Internet Not Connected",
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    private boolean validatePhone() {
        if (!et_input_phone.getText().toString().matches("[0][3][0-9]{9}|[3][0-9]{9}")) {
            et_input_phone.setError("Invalid Phone Number");
            requestFocus(et_input_phone);
            return false;
        } else if (et_input_phone.getText().toString().length() < 10) {
            et_input_phone.setError("Invalid Length");
            requestFocus(et_input_phone);
            return false;
        } else if (et_input_phone.getText().toString().trim().isEmpty()) {
            et_input_phone.setError("Phone Number Cannot Be Empty");
            requestFocus(et_input_phone);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void sendData() {

        ProgressDialogClass.showProgress(getActivity());

        phone = et_input_phone.getText().toString();
        pass = et_input_password.getText().toString();

        if (phone.length() == 10) {
            phone = "+92" + phone;
        } else {
            phone = "+92" + phone.substring(1);
        }
//        Toast.makeText(this, "" + phone + "" + pass, Toast.LENGTH_LONG).show();

        AndroidNetworking.post(ConfigURL.URL_LOGIN_WORKER)
                .addBodyParameter("pMobile", phone)
                .addBodyParameter("pPass", pass)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();

                        String pName = "", pEmail = "";
                        try

                        {
                            String msg = response.getString("message");
                            boolean error = false;

                            error = response.getBoolean("error");

                            if (!error) {

                                JSONArray jsonArray = response.getJSONArray("worker");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Worker workerDetail = new Worker(jsonArray.getJSONObject(i));
                                    pName = workerDetail.getWorker_name();
                                    pEmail = workerDetail.getWorker_email();
                                }

//                                RequestUrlForNavigationDrawerActivity.setWokerImage(WorkerSignInPage.this);

                                Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getActivity(), DrawerMainActivity.class);
                                startActivity(intent);
                                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("LOGIN", LOGIN);
                                editor.putString("PHONE", phone);
                                editor.putString("NAME", pName);
                                editor.putString("EMAIL", pEmail);
                                editor.commit();

                                //TODO uncommit this
                                SendRegistrationTokenFCM.sendRegistrationToServer(getActivity(), FirebaseInstanceId.getInstance().getToken());

//                                Toast.makeText(WorkerSignInPage.this, " "+LOGIN+" "+phone+" "+pName+" "+pEmail +" ", Toast.LENGTH_LONG).show();
                            }
                            if (error) {
                                ProgressDialogClass.hideProgress();
                                Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                            }
                        } catch (
                                JSONException e)

                        {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        Log.v("Sign In", "" + error);
//                        Toast.makeText(WorkerSignInPage.this, "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onClick(View view) {

        Fragment fragmentName = null;
        Fragment ForgotPasswordFragment = new ForgotPasswordFragment();
        Fragment SignUpInfoFragment = new SignUpInfoFragment();


        switch (view.getId()) {
            case R.id.btn_signin:
                submit();
                break;
            case R.id.tv_forgot_pass:
                fragmentName = ForgotPasswordFragment;
                replaceFragment(fragmentName);
                break;
            case R.id.tv_new_account:
                fragmentName = SignUpInfoFragment;
                replaceFragment(fragmentName);
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_signin, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}
