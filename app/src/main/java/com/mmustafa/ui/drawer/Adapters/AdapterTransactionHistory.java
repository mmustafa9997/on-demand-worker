package com.mmustafa.ui.drawer.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mmustafa.ui.drawer.Models.WorkerTransactionHistory;
import com.mmustafa.ui.drawer.R;

import java.util.ArrayList;


public class AdapterTransactionHistory extends RecyclerView.Adapter<AdapterTransactionHistory.MyViewHolder> {

    private static OnItemClickListener listener;
    private ArrayList<WorkerTransactionHistory> arrayList;
    private Context acontext;

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CardView mCardView;
        public TextView tv_transaction_job_cost, tv_transaction_amount_transfer, tv_transaction_amount_balance, tv_transaction_jobId,tv_transaction_updatTime;



        public MyViewHolder(View v) {
            super(v);

            mCardView = (CardView) v.findViewById(R.id.card_view_transaction);
            tv_transaction_jobId = (TextView) v.findViewById(R.id.tv_transaction_jobId);
            tv_transaction_job_cost = (TextView) v.findViewById(R.id.tv_transaction_job_cost);
            tv_transaction_amount_transfer = (TextView) v.findViewById(R.id.tv_transaction_amount_transfer);
            tv_transaction_amount_balance = (TextView) v.findViewById(R.id.tv_transaction_amount_balance);
             tv_transaction_updatTime = (TextView) v.findViewById(R.id.tv_transaction_updateTime);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null)
                listener.onItemClick(view, getLayoutPosition());
            Log.d("Tag", "clicked");
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);

    }

    public AdapterTransactionHistory(Context context, ArrayList<WorkerTransactionHistory> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterTransactionHistory.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_transactions, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        WorkerTransactionHistory current = arrayList.get(position);
        holder.tv_transaction_jobId.setText(current.getJobId());
        holder.tv_transaction_job_cost.setText(current.getAmount());
        holder.tv_transaction_amount_transfer.setText(current.getTransfer());
        holder.tv_transaction_amount_balance.setText(current.getBalance());
        holder.tv_transaction_updatTime.setText(current.getUpdateTime());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

}
