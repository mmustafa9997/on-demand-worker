package com.mmustafa.ui.drawer.SignIn;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.DrawerMainActivity;
import com.mmustafa.ui.drawer.R;
import com.mmustafa.ui.drawer.utils.NetworkConnectivityClass;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotInputNewPasswordFragment extends Fragment implements View.OnClickListener {

    Button btn_done;
    EditText et_input_pass, et_input_confirm_pass;
    String phone, pass;

    public ForgotInputNewPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_forgot_input_new_password, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        Bundle bundle = getArguments();

        if (bundle != null) {
            phone = (String) bundle.get("phone");
        }

        btn_done = (Button) rootView.findViewById(R.id.btn_done);
        et_input_pass = (EditText) rootView.findViewById(R.id.et_input_pass);
        et_input_confirm_pass = (EditText) rootView.findViewById(R.id.et_input_confirm_pass);
        btn_done.setOnClickListener(this);

        return rootView;
    }


    public void submit() {

        if (!validatePassword()) {
            return;
        }
        if (!validatePasswordConfirmation()) {
            return;
        }
        if (NetworkConnectivityClass.isNetworkAvailable(getActivity())) {
            sendData();
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), "Internet Not Connected",
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    private boolean validatePassword() {
        if (et_input_pass.getText().toString().trim().isEmpty()) {
            et_input_pass.setError("Password Cannot Be Empty");
            requestFocus(et_input_pass);
            return false;
        }
        return true;
    }

    private boolean validatePasswordConfirmation() {
        if (et_input_confirm_pass.getText().toString().trim().isEmpty()) {
            et_input_confirm_pass.setError("Confirm Password Cannot Be Empty");
            requestFocus(et_input_confirm_pass);
            return false;
        } else if (!et_input_pass.getText().toString().equals(et_input_confirm_pass.getText().toString())) {
            et_input_pass.setError("Password Not Match");
            requestFocus(et_input_pass);
            return false;
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void sendData() {

        pass = et_input_pass.getText().toString();

        Toast.makeText(getActivity(), "" + phone + "" + pass, Toast.LENGTH_LONG).show();

        AndroidNetworking.post(ConfigURL.URL_FORGOT_WORKER_PASS)
                .addBodyParameter("pMobile", phone)
                .addBodyParameter("pPass", pass)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String msg = response.getString("message");
                            boolean error = false;

                            error = response.getBoolean("error");

                            if (!error) {
                                Intent intent = new Intent(getActivity(), DrawerMainActivity.class);
                                intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                getActivity().finish();
                            }
                            if (error) {
                                Toast.makeText(getActivity(), "" + msg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getActivity(), "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_done:
                submit();
                break;

        }
    }
}
