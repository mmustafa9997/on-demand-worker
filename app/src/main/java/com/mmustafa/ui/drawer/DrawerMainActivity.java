package com.mmustafa.ui.drawer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mmustafa.ui.drawer.TabLayout.TabLayoutMain;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.utils.RequestUrlForNavigationDrawerActivity;
import com.squareup.picasso.Picasso;

public class DrawerMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    Button bt_worker_online, bt_worker_offline;
    int MyVersion = Build.VERSION.SDK_INT;
    ImageView imageView;
    TextView tv_nav_head_name, tv_nav_head_mobile;
    boolean checkPermission = false;


    String latitude, longitude, title, address, jobType, jobId, mobileNo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //set mobile and name in nav head
        tv_nav_head_name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_nav_head_name);
        tv_nav_head_name.setText(ConfigURL.getWorkerName(this));

        tv_nav_head_mobile = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_nav_head_mobile);
        tv_nav_head_mobile.setText(ConfigURL.getMobileNumber(this));

        imageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);

        String imagePath = ConfigURL.getWorkerProfilePic(this);
        if (!imagePath.isEmpty()) {
            Picasso.with(this).load(imagePath).resize(80, 80).error(R.drawable.ic_person_outline_white_48dp).into(imageView);
        }
        RequestUrlForNavigationDrawerActivity.setWokerImage(this);

        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                +WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                +WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        checkIfAlreadyPermission();


        bt_worker_online = (Button) findViewById(R.id.bt_worker_online);
        bt_worker_offline = (Button) findViewById(R.id.bt_worker_offline);

    }

    public void openFragment() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            latitude = (String) bundle.get("latitude");
            longitude = (String) bundle.get("longitude");
            title = (String) bundle.get("title");
            address = (String) bundle.get("address");
            jobId = (String) bundle.get("jobId");
            jobType = (String) bundle.get("type");
            mobileNo = (String) bundle.get("customerMobileNumber");

            Fragment fragmentName = null;
            Fragment MapFragment = new MapFragment();
            fragmentName = MapFragment;

            Bundle args = new Bundle();

            args.putString("latitude", latitude);
            args.putString("longitude", longitude);
            args.putString("title", title);
            args.putString("address", address);
            args.putString("jobId", jobId);
            args.putString("type", jobType);
            args.putString("customerMobileNumber", mobileNo);

            fragmentName.setArguments(args);
            replaceFragment(fragmentName);


        } else {

            Fragment fragmentName = null;
            Fragment MapFragment = new MapFragment();
            fragmentName = MapFragment;
            replaceFragment(fragmentName);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            }
            /*if (ConfigURL.getOnStatus(DrawerMainActivity.this).equals("ONLINE")) {
                Toast.makeText(this, "Please Go Offline Or Press Home Button", Toast.LENGTH_LONG).show();
            } */
            else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment fragmentName = null;

        Fragment MapFragment = new MapFragment();
        Fragment TabLayoutMain = new TabLayoutMain();
        Fragment TransactionHistory = new TransactionHistory();
        Fragment ProfileFragment = new ProfileFragment();
        Fragment AboutUsFragment = new AboutUsFragment();
        Fragment ReportIssueFragment = new ReportIssueFragment();

        int id = item.getItemId();

        if (id == R.id.home_user) {
            fragmentName = MapFragment;
            replaceFragment(fragmentName);

        } else if (id == R.id.myorder_user) {
            hideButton();
            fragmentName = TabLayoutMain;
            replaceFragment(fragmentName);

        } else if (id == R.id.transactions_user) {
            hideButton();
            fragmentName = TransactionHistory;
            replaceFragment(fragmentName);

        } else if (id == R.id.setting) {
            hideButton();
            fragmentName = ProfileFragment;
            replaceFragment(fragmentName);

        } else if (id == R.id.nav_about_us) {
            hideButton();
            fragmentName = AboutUsFragment;
            replaceFragment(fragmentName);

        } else if (id == R.id.help_us_user) {
            hideButton();
            fragmentName = ReportIssueFragment;
            replaceFragment(fragmentName);

        } else if (id == R.id.nav_log_out) {
            Intent intent = new Intent(this, SignInActivity.class);
            intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
            stopService(new Intent(DrawerMainActivity.this, MyService.class));
            startActivity(intent);
            ConfigURL.clearshareprefrence(this);
            finish();
        }
        // moveToFragment(fragment);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }

    public void hideButton() {
        bt_worker_online.setVisibility(View.GONE);
        bt_worker_offline.setVisibility(View.GONE);
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;


        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = false;
        try {

            fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        } catch (IllegalStateException ignored) {
            // There's no way to avoid getting this if saveInstanceState has already been called.
        }
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_dashboard, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commitAllowingStateLoss();
        }

    }

    //Location Permissions
    private void checkIfAlreadyPermission() {
        if (ContextCompat.checkSelfPermission(DrawerMainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);

        } else {
            GPSandPermissionCheck();
        }
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(DrawerMainActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else {
            // Permission has already been granted
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
               /* // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //GPSandPermissionCheck();
                    openFragment();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            1);
                }*/


                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        openFragment();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            1);
                }


                return;
            }
        }
    }

    private void GPSandPermissionCheck() {
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            buildAlertMessageNoGps();
        } else
            openFragment();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Need location services");
        builder.setMessage("Please enable location services.")
                .setCancelable(false)
                .setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        //openFragment();
                    }
                })
                .setNegativeButton("LATER", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        buildAlertMessageNoGps();
                        dialog.dismiss();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

}
