package com.mmustafa.ui.drawer;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.utils.ProgressDialogClass;
import com.mmustafa.ui.drawer.utils.RequestUrlForNavigationDrawerActivity;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class JobFinishFragment extends Fragment implements View.OnClickListener {

    TextView tv_new_job_title, tv_new_job_hours;
    EditText et_job_finish_amount;
    TextView et_job_finish_fare;
    RatingBar rate_customer;
    Button btn_finish_job_url;
    boolean isError = true;
    String title;

    public JobFinishFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_job_finish, container, false);

        title = ConfigURL.getJobTitle(getActivity());

        tv_new_job_title = (TextView) rootView.findViewById(R.id.tv_new_job_title);
        et_job_finish_fare = (TextView) rootView.findViewById(R.id.et_job_finish_fare);
        et_job_finish_amount = (EditText) rootView.findViewById(R.id.et_job_finish_amount);
        rate_customer = (RatingBar) rootView.findViewById(R.id.rate_customer);
        btn_finish_job_url = (Button) rootView.findViewById(R.id.btn_finish_job_url);
        tv_new_job_hours = (TextView) rootView.findViewById(R.id.tv_new_job_hours);
        btn_finish_job_url.setOnClickListener(this);

        tv_new_job_title.setText(title);
        RequestUrlForNavigationDrawerActivity.setCurrentJobTime(getActivity(), ConfigURL.getJobID(getActivity()), tv_new_job_hours, et_job_finish_fare);


        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_finish_job_url:

                if (et_job_finish_amount.getText().toString().isEmpty()) {
                    et_job_finish_amount.setError("Amount Cann't be Empty");
                    requestFocus(et_job_finish_fare);
                    return;
                }
                if (rate_customer.getRating() == 0.0) {
                    Toast.makeText(getActivity(), "Please Rate Customer Experience", Toast.LENGTH_LONG).show();
                    return;
                }

                workerJobFinish(getActivity(), ConfigURL.getJobID(getActivity()), et_job_finish_amount.getText().toString(), Float.toString(rate_customer.getRating()));

                break;
        }

    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_dashboard, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public void workerJobFinish(Context context, String jobId, String jobCashRecieved, String rateByWorker) {
        ProgressDialogClass.showProgress(context);

        AndroidNetworking.post(ConfigURL.URL_JOB_WORKER_FINISH)
                .addBodyParameter("jobId", jobId.toString())
                .addBodyParameter("jobCashRecieved", jobCashRecieved.toString())
                .addBodyParameter("rateByWorker", rateByWorker.toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            response.getString("message");
                            if (response.getBoolean("error")) {
                                Log.v("Job Finish Fragment", "FINISH NOT DONE");
                            } else {
                                ProgressDialogClass.hideProgress();
                                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", 0);
                                preferences.edit().remove("JOBID");
                                preferences.edit().remove("ADDRESS");
                                preferences.edit().remove("TITLE");
                                preferences.edit().remove("LAT");
                                preferences.edit().remove("LNG");
                                preferences.edit().remove("CUSTOMERMOBILE");
                                preferences.edit().commit();

                                getActivity().startService(new Intent(getActivity(), MyService.class));

                                getActivity().onBackPressed();
                                Log.v("Job Finish Fragment", "FINISH DONE");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        Log.e("Job Finish Fragment", "Finish Job Error " + error);

                    }
                });
    }
}
