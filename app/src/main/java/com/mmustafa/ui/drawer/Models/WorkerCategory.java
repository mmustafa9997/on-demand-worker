package com.mmustafa.ui.drawer.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Qasim Ahmed on 18/12/2017.
 */

public class WorkerCategory {
    String categoryId;
    String categoryName;


    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public WorkerCategory(String categoryId, String categoryName) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public WorkerCategory(JSONObject jsonObject) {
        try {
            this.categoryId = jsonObject.getString("categoryId");
            this.categoryName = jsonObject.getString("categoryname");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
