package com.mmustafa.ui.drawer.Models;

import org.json.JSONException;
import org.json.JSONObject;


public class WorkerJobStatus {
    String jobId;
    String jobStatus;
    String Title;
    String latitude;
    String longitude;
    String CustomerMobileNo;

    public WorkerJobStatus(String jobId, String jobStatus, String title, String latitude, String longitude, String customerMobileNo) {
        this.jobId = jobId;
        this.jobStatus = jobStatus;
        this.Title = title;
        this.latitude = latitude;
        this.longitude = longitude;
        this.CustomerMobileNo = customerMobileNo;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCustomerMobileNo() {
        return CustomerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        CustomerMobileNo = customerMobileNo;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public WorkerJobStatus(JSONObject jsonObject)
    {
        try {
            this.jobId = jsonObject.getString("jobId");
            this.jobStatus = jsonObject.getString("jobStatus");
            this.Title = jsonObject.getString("Title");
            this.latitude = jsonObject.getString("latitude");
            this.longitude = jsonObject.getString("longitude");
            this.CustomerMobileNo = jsonObject.getString("CustomerMobileNo");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
