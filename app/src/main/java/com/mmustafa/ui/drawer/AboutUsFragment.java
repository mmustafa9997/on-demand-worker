package com.mmustafa.ui.drawer;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment implements View.OnClickListener {

    TextView like_us_on_fb, follow_us_on_twitter, share_with_your_friend, rate_us_on_play_store;

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about_us, container, false);

        getActivity().setTitle("About Us");

        like_us_on_fb = (TextView) rootView.findViewById(R.id.like_us_on_fb);
        follow_us_on_twitter = (TextView) rootView.findViewById(R.id.follow_us_on_twitter);
        share_with_your_friend = (TextView) rootView.findViewById(R.id.share_with_your_friend);
        rate_us_on_play_store = (TextView) rootView.findViewById(R.id.rate_us_on_play_store);

        like_us_on_fb.setOnClickListener(this);
        follow_us_on_twitter.setOnClickListener(this);
        share_with_your_friend.setOnClickListener(this);
        rate_us_on_play_store.setOnClickListener(this);

        return rootView;
    }

    public void rateUsOnGooglePlay() {
        Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
        }
    }

    public void share_with_your_friend() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "یہ موبائل ایپلیکیشن میرے لئے کام تلاش کرنے کے لئے مجھے بہت مدد کرتا ہے";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public void openTwitterIntent() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + getActivity().getPackageName())));
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + getActivity().getPackageName())));
        }
    }

    public void openFacebookIntent() {

        try {
            getActivity().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/<id_here>")));
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/<user_name_here>")));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.like_us_on_fb:
                openFacebookIntent();
                break;
            case R.id.follow_us_on_twitter:
                openTwitterIntent();
                break;
            case R.id.share_with_your_friend:
                share_with_your_friend();
                break;
            case R.id.rate_us_on_play_store:
                rateUsOnGooglePlay();
                break;
        }
    }
}
