package com.mmustafa.ui.drawer;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileFragment extends Fragment {

    TextView tv_change_password, tv_worker_name, tv_worker_phone, tv_worker_email;
    CircleImageView profile_image;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        getActivity().setTitle("Profile");

        tv_worker_name = (TextView) rootView.findViewById(R.id.tv_worker_name);
       tv_worker_name.setText(ConfigURL.getWorkerName(getActivity()));
        tv_worker_phone = (TextView) rootView.findViewById(R.id.tv_worker_phone);
        tv_worker_phone.setText(ConfigURL.getMobileNumber(getActivity()));
        tv_worker_email = (TextView) rootView.findViewById(R.id.tv_worker_email);
        tv_worker_email.setText(ConfigURL.getWorkerEmail(getActivity()));
        profile_image = (CircleImageView) rootView.findViewById(R.id.profile_image);

        String imagePath = ConfigURL.getWorkerProfilePic(getActivity());
        if (!imagePath.isEmpty()) {
            Picasso.with(getActivity()).load(imagePath).resize(200, 200).error(R.drawable.ic_person_white).into(profile_image);
            // Toast.makeText(this,ConfigURL.getWorkerProfilePic(this),Toast.LENGTH_SHORT).show();
        }

        tv_change_password = (TextView) rootView.findViewById(R.id.tv_change_password);

        tv_change_password.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Fragment fragmentName = null;
                Fragment ProfilePasswordChangeFragment = new ProfilePasswordChangeFragment();
                fragmentName = ProfilePasswordChangeFragment;
                replaceFragment(fragmentName);
            }
        });

        return rootView;
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_dashboard, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

}
