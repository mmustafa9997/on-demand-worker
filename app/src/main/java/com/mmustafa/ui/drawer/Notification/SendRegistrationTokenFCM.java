package com.mmustafa.ui.drawer.Notification;


import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.utils.ConfigURL;

import org.json.JSONObject;

public class SendRegistrationTokenFCM {

    public static void sendRegistrationToServer(Context context, String token) {

        AndroidNetworking.post(ConfigURL.URL_SEND_TOKEN)
                .addBodyParameter("apiKey", token)
                .addBodyParameter("workerMobileNo", ConfigURL.getMobileNumber(context))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.v("FIREBASE:result",""+response);
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.v("FIREBASE:error",""+error);
                    }
                });
    }
}
