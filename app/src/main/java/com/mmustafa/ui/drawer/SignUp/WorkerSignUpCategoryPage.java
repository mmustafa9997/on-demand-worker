package com.mmustafa.ui.drawer.SignUp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.Models.WorkerCategory;
import com.mmustafa.ui.drawer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorkerSignUpCategoryPage extends Fragment implements View.OnClickListener {

    String full_name, email, phone, gender, categoryList = "", categoryId = "";
    List<WorkerCategory> data;
    String[] arrayCategoryName;
    String[] arrayCategoryId;
    HashMap<String, String> meMap = new HashMap<String, String>();
    byte[] byteArray;
    Button btn_next;
    ListView listView;
    String[] city = {
            "Plumber",
            "Carpenter",
            "Painter",
            "Cleaning",
            "Electrician",
            "Car Service"
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.worker_signup_category_selection, container, false);

        listView = (ListView) rootView.findViewById(R.id.list_view_category);
        btn_next = (Button) rootView.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);

        Bundle bundle = getArguments();

        if (bundle != null) {
            full_name = (String) bundle.get("full_name");
            phone = (String) bundle.get("phone");
            email = (String) bundle.get("email");
            gender = (String) bundle.get("gender");
            byteArray = getArguments().getByteArray("image");
        }

        listView.setChoiceMode(listView.CHOICE_MODE_SINGLE);
        listView.setTextFilterEnabled(true);


        AndroidNetworking.get(ConfigURL.URL_GET_CATEGORY_FOR_REGISTRATION)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        data = new ArrayList<WorkerCategory>();
                        try {
                            JSONArray jsonArray = response.getJSONArray("category");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String categoryname, categoryId;

                                WorkerCategory category = new WorkerCategory(jsonArray.getJSONObject(i));
                                categoryId = category.getCategoryId();
                                categoryname = category.getCategoryName();

                                WorkerCategory obj = new WorkerCategory(categoryId, categoryname);
                                meMap.put(categoryname, categoryId);
                                data.add(obj);
                            }
                            arrayCategoryName = new String[data.size()];

                            for (int i = 0; i < data.size(); i++) {
                                arrayCategoryName[i] = data.get(i).getCategoryName();
                                Log.v("WorkerSignUp", "" + arrayCategoryName[i]);
                            }
                            listView.setAdapter(new ArrayAdapter<String>(getActivity(),
                                    android.R.layout.simple_list_item_checked, arrayCategoryName));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                    }
                });

        return rootView;
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        CheckedTextView item = (CheckedTextView) v;

//        Toast.makeText(this, arrayCategoryName[position] + " checked : " +
//                item.isChecked(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:

                SparseBooleanArray checked = listView.getCheckedItemPositions();

                for (int i = 0; i < listView.getAdapter().getCount(); i++) {
                    if (checked.get(i)) {
                        // categoryList = listView.getItemAtPosition(i).toString();
                        //categoryList= listView.getItemAtPosition(i).toString();
                        categoryList = meMap.get(listView.getItemAtPosition(i).toString());
                        // category id prints here
                       // Toast.makeText(getActivity(),""+meMap.get(listView.getItemAtPosition(i).toString()),Toast.LENGTH_LONG).show();
                    }
                }

                Fragment fragmentName = null;
                Fragment SignUpPassAndConfirmPassFragment = new SignUpPassAndConfirmPassFragment();
                fragmentName = SignUpPassAndConfirmPassFragment;

                Bundle args = new Bundle();
                args.putString("full_name", full_name);
                args.putString("email", email);
                args.putString("phone", phone);
                args.putString("gender", gender);
                args.putByteArray("image", byteArray);
                args.putString("category", categoryList);

                fragmentName.setArguments(args);
                replaceFragment(fragmentName);

                /*Intent intent = new Intent(WorkerSignUpCategoryPage.this, WorkerSignUpInputPasswordAndConfirmationPassword.class);
                intent.putExtra("full_name", full_name);
                intent.putExtra("email", email);
                intent.putExtra("phone", phone);
                intent.putExtra("gender", gender);
                intent.putExtra("image", byteArray);
                intent.putExtra("category", categoryList);
                startActivity(intent);*/
//                Toast.makeText(getActivity(), "Category" + categoryList + "Full Name" + full_name + "Email" + email + "Phone" + phone + "Gender" + gender+"ByteArray "+byteArray, Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_signin, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}