package com.mmustafa.ui.drawer.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mmustafa.ui.drawer.CompletedJobDetailFragment;
import com.mmustafa.ui.drawer.Models.WorkerJobs;
import com.mmustafa.ui.drawer.R;

import java.util.ArrayList;


public class AdapterJobCompleted extends RecyclerView.Adapter<AdapterJobCompleted.MyViewHolder> {

    private Context acontext;
    private ArrayList<WorkerJobs> arrayList;
    private AdapterJobCompleted.ItemClickListener clickListener;


    public interface ItemClickListener {
        void onClick(View view, int position);
    }

    public void setClickListener(AdapterJobCompleted.ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CardView mCardView;
        public TextView tv_completed_job_category, tv_completed_job_customer_name, tv_completed_job_date_time, tv_completed_job_cost,tv_completed_job_bookingId;

        public MyViewHolder(View v) {
            super(v);

            mCardView = (CardView) v.findViewById(R.id.card_view_order_completed);
            tv_completed_job_category = (TextView) v.findViewById(R.id.tv_completed_job_category);
            tv_completed_job_customer_name = (TextView) v.findViewById(R.id.tv_completed_job_customer_name);
            tv_completed_job_date_time = (TextView) v.findViewById(R.id.tv_completed_job_date_time);
            tv_completed_job_cost = (TextView) v.findViewById(R.id.tv_completed_job_cost);
            tv_completed_job_bookingId = (TextView) v.findViewById(R.id.tv_completed_job_bookingId);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }


    public AdapterJobCompleted(Context context, ArrayList<WorkerJobs> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }


    @Override
    public AdapterJobCompleted.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_order_completed, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        WorkerJobs current = arrayList.get(position);
        holder.tv_completed_job_category.setText(current.getJobTitle());
        holder.tv_completed_job_customer_name.setText(current.getCutomerName());
        holder.tv_completed_job_date_time.setText(current.getJobCreationTime());
        holder.tv_completed_job_cost.setText("Rs. " + current.getJobCost());
        holder.tv_completed_job_bookingId.setText(current.getJobId());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

}
