package com.mmustafa.ui.drawer.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Muhammad Mustafa on 10/12/2017.
 */

public class WorkerLogin {
    String worker_phone;
    String worker_pass;

    public WorkerLogin(String worker_phone, String worker_pass) {
        this.worker_phone = worker_phone;
        this.worker_pass = worker_pass;
    }

    public String getWorker_phone() {
        return worker_phone;
    }

    public void setWorker_phone(String worker_phone) {
        this.worker_phone = worker_phone;
    }

    public String getWorker_pass() {
        return worker_pass;
    }

    public void setWorker_pass(String worker_pass) {
        this.worker_pass = worker_pass;
    }

    public WorkerLogin(JSONObject jsonObject)
    {
        try {
            this.worker_phone = jsonObject.getString("pMobile");
            this.worker_pass = jsonObject.getString("pPass");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
