package com.mmustafa.ui.drawer;


import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mmustafa.ui.drawer.Notification.SendRegistrationTokenFCM;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.utils.NetworkConnectivityClass;
import com.mmustafa.ui.drawer.utils.RequestUrlForNavigationDrawerActivity;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener, RequestUrlForNavigationDrawerActivity.MyCallback {

    TextView tv_new_job_activity_title, tv_new_job_activity_timer, tv_new_job_address, tv_new_job_mobile;
    Button btn_reject, btn_accept, btn_arrived, btn_start, btn_finish, btn_worker_offline, btn_worker_online, btn_cancel_job;
    LinearLayout ll_layout_job_detial, ll_layout_job_accept_reject, ll_layout_job_arrived, ll_layout_job_start, ll_layout_job_finish;

    Geocoder geocoder;
    List<Address> addresses;

    boolean isError = false;

    String latitude, longitude, title, address, jobType, jobId, mobileNo;

    //Countdown TImer
    CountDownTimer timer;

    //For Google Map
    MapView mMapView;
    View mView;
    GoogleMap mGoogleMap;
    Marker mCurrLocationMarker;
    Double lat, lng;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            jobType = intent.getStringExtra("type");

            Log.d("Navigation ", "" + jobType);

            if (jobType == null) {
                return;
            }
            if (jobType.toString().equals("2")) {
                // Promotion
                Toast.makeText(getActivity(), "Promotion Notification Received", Toast.LENGTH_LONG).show();

            } else if (jobType.toString().equals("0")) {
                //JOb Cancellation Screen
                if (MapFragment.this.getUserVisibleHint()) {
                    //getActivity().onBackPressed();
                    clearBackStack();
                    showAlertDialogForCancelJob();
                }
            } else if (jobType.toString().equals("1")) {
                // Incoming Job
                updateLayout("INCOMING");
            } else if (jobType.toString().equals("3")) {

                address = getAddress(lat, lng);
                SharedPreferences preference = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preference.edit();
                editor.putString("ADDRESS", address);
                editor.commit();

                btn_start.setClickable(true);


                isError = RequestUrlForNavigationDrawerActivity.workerJobStart(getActivity(), jobId);
                if (isError)
                    return;

                updateLayout("START");

            }
            //  cancelNotification(getActivity(), 0);
        }
    };

    public MapFragment() {
        // Required empty public constructor
    }

    public static void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btn_worker_online = (Button) getActivity().findViewById(R.id.bt_worker_online);
        btn_worker_online.setOnClickListener(this);

        btn_worker_offline = (Button) getActivity().findViewById(R.id.bt_worker_offline);
        btn_worker_offline.setOnClickListener(this);

        btn_cancel_job = (Button) getActivity().findViewById(R.id.btn_cancel_job);
        btn_cancel_job.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_map, container, false);
        getActivity().setTitle("Dashboard");

        RequestUrlForNavigationDrawerActivity req = new RequestUrlForNavigationDrawerActivity();
        req.checkWorkerJobStatus(getActivity());
        req.addListener(this);

        //TODO Set visibility from shared preference
        ll_layout_job_detial = (LinearLayout) mView.findViewById(R.id.ll_layout_job_detial);
        tv_new_job_activity_title = (TextView) mView.findViewById(R.id.tv_new_job_activity_title);
        tv_new_job_activity_timer = (TextView) mView.findViewById(R.id.tv_new_job_activity_timer);
        tv_new_job_address = (TextView) mView.findViewById(R.id.tv_new_job_address);
        tv_new_job_mobile = (TextView) mView.findViewById(R.id.tv_new_job_mobile);

        ll_layout_job_accept_reject = (LinearLayout) mView.findViewById(R.id.ll_layout_job_accept_reject);
        btn_accept = (Button) mView.findViewById(R.id.btn_accept);
        btn_accept.setOnClickListener(this);
        btn_reject = (Button) mView.findViewById(R.id.btn_reject);
        btn_reject.setOnClickListener(this);

        ll_layout_job_arrived = (LinearLayout) mView.findViewById(R.id.ll_layout_job_arrived);
        btn_arrived = (Button) mView.findViewById(R.id.btn_arrived);
        btn_arrived.setOnClickListener(this);

        ll_layout_job_start = (LinearLayout) mView.findViewById(R.id.ll_layout_job_start);
        btn_start = (Button) mView.findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);

        ll_layout_job_finish = (LinearLayout) mView.findViewById(R.id.ll_layout_job_finish);
        btn_finish = (Button) mView.findViewById(R.id.btn_finish);
        btn_finish.setOnClickListener(this);


        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = (MapView) mView.findViewById(R.id.map);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

        Bundle bundle = getArguments();

        if (bundle != null) {

            jobType = (String) bundle.get("type");

            if (jobType == null) {
                return;
            }
            if (jobType.toString().equals("2")) {
                // Promotion
                Toast.makeText(getActivity(), "Promotion Notification Received", Toast.LENGTH_LONG).show();

            } else if (jobType.toString().equals("0")) {
                //JOb Cancellation Screen
                showAlertDialogForCancelJob();

            } else if (jobType.toString().equals("1")) {
                // Incoming Job
                updateLayout("INCOMING");
            } else if (jobType.toString().equals("3")) {
                address = getAddress(lat, lng);
                SharedPreferences preference = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preference.edit();
                editor.putString("CLICKABLE", "true");
                editor.putString("ADDRESS", address);
                editor.commit();

                btn_start.setClickable(true);

                isError = RequestUrlForNavigationDrawerActivity.workerJobStart(getActivity(), jobId);
                if (isError)
                    return;

                updateLayout("START");

            }
        }

        if (ConfigURL.getOnStatus(getActivity()).equals("ONLINE")) {

            hideButton(View.VISIBLE, View.GONE, View.GONE);
        } else if (ConfigURL.getOnStatus(getActivity()).equals("OFFLINE")) {
            hideButton(View.GONE, View.VISIBLE, View.GONE);

        } else {
            hideButton(View.GONE, View.VISIBLE, View.GONE);
        }


    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        MapsInitializer.initialize(getContext());
        mGoogleMap.setMyLocationEnabled(true);

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        @SuppressLint("MissingPermission") Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null) {
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17));

        }
    }

    public void setMarkerOfCustomer(Double mLat, Double mLng) {

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_icons8_home_address);

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        LatLng latLng = new LatLng(mLat, mLng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(title);
        markerOptions.icon(icon);
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);
        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));

    }

    @SuppressLint({"NewApi", "ResourceType"})
    @Override
    public void onClick(View view) {

        jobId = ConfigURL.getJobID(getActivity());

        switch (view.getId()) {

            case R.id.bt_worker_online:
                if (NetworkConnectivityClass.isNetworkAvailable(getActivity())) {

                    btn_worker_online.setVisibility(View.GONE);
                    btn_worker_offline.setVisibility(View.VISIBLE);

                    SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("ONSTATUS", "ONLINE");
                    editor.commit();

                    //Start Service
                    getActivity().startService(new Intent(getActivity(), MyService.class));
                    //Update FCM Key
                    SendRegistrationTokenFCM.sendRegistrationToServer(getActivity(), FirebaseInstanceId.getInstance().getToken());
                    //For Online
                    customNotification();

                } else {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Internet Not Connected",
                            Snackbar.LENGTH_SHORT).show();
                }
                break;

            case R.id.bt_worker_offline:
                if (NetworkConnectivityClass.isNetworkAvailable(getActivity())) {

                    btn_worker_offline.setVisibility(View.GONE);
                    btn_worker_online.setVisibility(View.VISIBLE);

                    SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("ONSTATUS", "OFFLINE");
                    editor.commit();

                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }

                    getActivity().stopService(new Intent(getActivity(), MyService.class));
                    cancelNotification(getActivity(), 111);

               /*     WallpaperManager myWallpaper
                            = WallpaperManager.getInstance(getActivity());
                    myWallpaper.getWallpaperId();*/


                    Snackbar.make(getActivity().findViewById(android.R.id.content), "You Are Offline",
                            Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Internet Not Connected",
                            Snackbar.LENGTH_SHORT).show();
                }
                break;

            case R.id.btn_accept:
                timer.cancel();
                isError = RequestUrlForNavigationDrawerActivity.sendJobAcceptance(getActivity(), jobId, "1");
                if (isError)
                    return;
                //Toast.makeText(getActivity(), "" + jobId, Toast.LENGTH_SHORT).show();


                updateLayout("ACTIVE");

                break;

            case R.id.btn_reject:
                timer.cancel();
                isError = RequestUrlForNavigationDrawerActivity.sendJobAcceptance(getActivity(), jobId, "0");
                if (isError)
                    return;
                updateLayout("REJECT");
                break;

            case R.id.btn_arrived:
                isError = RequestUrlForNavigationDrawerActivity.workerArrived(getActivity(), jobId);
                if (isError)
                    return;
                updateLayout("ARRIVED");

                getActivity().stopService(new Intent(getActivity(), MyService.class));

                break;

            case R.id.btn_start:


                acceptableAmountDialog();
                /*isError = RequestUrlForNavigationDrawerActivity.workerJobStart(getActivity(), jobId);
                if (isError)
                    return;
                updateLayout("START");*/

                break;

            case R.id.btn_finish:
                updateLayout("FINISH");
                break;

            case R.id.btn_cancel_job:
                updateLayout("CANCEL");

                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            getActivity().registerReceiver(mMessageReceiver, new IntentFilter("com.google.android.c2dm.intent.RECEIVE"));
        } catch (Exception e) {
            // This will catch any exception, because they are all descended from Exception
            System.out.println("Error " + e.getMessage());
        }
        if (ConfigURL.getOnStatus(getActivity()).equals("ONLINE")) {

            /*if (mRequestingLocationUpdates) {
                startLocationUpdates();
            }*/
            // registerReceiver(mMessageReceiver, new IntentFilter("com.google.android.c2dm.intent.RECEIVE"));
        } else {
            /*stopLocationUpdates();*/
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (!ConfigURL.getOnStatus(getActivity()).equals("ONLINE")) {
            //Toast.makeText(getActivity(),"You Are Offline ! ",Toast.LENGTH_LONG).show();
            /*if (mRequestingLocationUpdates) {
                startLocationUpdates();
            }*/
            /*stopLocationUpdates();*/

            try {
                getActivity().unregisterReceiver(mMessageReceiver);
            } catch (Exception e) {
                // This will catch any exception, because they are all descended from Exception
                System.out.println("Error " + e.getMessage());
            }

        } else {
            /*try {
                getActivity().unregisterReceiver(mMessageReceiver);
            } catch (Exception e) {
                // This will catch any exception, because they are all descended from Exception
                System.out.println("Error " + e.getMessage());
            }*/
            //stopLocationUpdates();
        }
    }

    public void updateLayout(String notificationType) {

        address = ConfigURL.getJobAddress(getActivity());
        latitude = ConfigURL.getJobLat(getActivity());
        longitude = ConfigURL.getJobLng(getActivity());
        title = ConfigURL.getJobTitle(getActivity());
        mobileNo = ConfigURL.getJobMobileNumber(getActivity());

        lat = Double.parseDouble(latitude);
        lng = Double.parseDouble(longitude);

        //Toast.makeText(getActivity(),""+address,Toast.LENGTH_LONG).show();

        switch (notificationType) {
            case "INCOMING":

                //TODO GONE CANCEL JOB BUTTON
                hideButton(View.GONE, View.GONE, View.GONE);

                ll_layout_job_detial.setVisibility(View.VISIBLE);
                ll_layout_job_accept_reject.setVisibility(View.VISIBLE);
                tv_new_job_activity_timer.setVisibility(View.VISIBLE);
                tv_new_job_address.setVisibility(View.VISIBLE);
                final NumberFormat f = new DecimalFormat("00");


                final int i = 0;
                timer = new CountDownTimer(60000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        tv_new_job_activity_timer.setText("00 : " + f.format((millisUntilFinished / 1000)));

                        try {
                            if (i == 0)
                                setMarkerOfCustomer(lat, lng);
                        } catch (Exception e) {
                            System.out.println("Error " + e.getMessage());
                        }
                    }

                    public void onFinish() {

                        updateLayout("REJECT");

                    }

                };
                timer.start();


                tv_new_job_address.setText(address);
                tv_new_job_activity_title.setText(title);

                //setMarkerOfCustomer(Double.parseDouble(latitude), Double.parseDouble(longitude));

                break;
            case "ACTIVE":

                hideButton(View.GONE, View.GONE, View.VISIBLE);

                tv_new_job_activity_timer.setVisibility(View.GONE);
                ll_layout_job_accept_reject.setVisibility(View.GONE);

                tv_new_job_address.setVisibility(View.VISIBLE);
                ll_layout_job_detial.setVisibility(View.VISIBLE);
                ll_layout_job_arrived.setVisibility(View.VISIBLE);
                tv_new_job_mobile.setVisibility(View.VISIBLE);
                tv_new_job_mobile.setText(mobileNo);
                tv_new_job_address.setText(address);
                tv_new_job_activity_title.setText(title);

                try {
                    setMarkerOfCustomer(lat, lng);
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage());
                }

                break;

            case "REJECT":

                hideButton(View.VISIBLE, View.GONE, View.GONE);

                tv_new_job_activity_timer.setVisibility(View.GONE);
                ll_layout_job_accept_reject.setVisibility(View.GONE);

                tv_new_job_address.setVisibility(View.GONE);
                ll_layout_job_detial.setVisibility(View.GONE);
                ll_layout_job_arrived.setVisibility(View.GONE);
                tv_new_job_mobile.setVisibility(View.GONE);


                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", 0);
                preferences.edit().remove("JOBID");
                preferences.edit().remove("ADDRESS");
                preferences.edit().remove("TITLE");
                preferences.edit().remove("LAT");
                preferences.edit().remove("LNG");
                preferences.edit().remove("CUSTOMERMOBILE");
                preferences.edit().commit();

                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                break;

            case "ARRIVED":

                hideButton(View.GONE, View.GONE, View.VISIBLE);

                tv_new_job_activity_timer.setVisibility(View.GONE);
                ll_layout_job_accept_reject.setVisibility(View.GONE);
                ll_layout_job_arrived.setVisibility(View.GONE);

                tv_new_job_address.setVisibility(View.VISIBLE);
                ll_layout_job_detial.setVisibility(View.VISIBLE);
                ll_layout_job_start.setVisibility(View.VISIBLE);
                tv_new_job_mobile.setVisibility(View.VISIBLE);
                tv_new_job_mobile.setText(mobileNo);
                tv_new_job_address.setText(address);
                tv_new_job_activity_title.setText(title);

                try {
                    setMarkerOfCustomer(lat, lng);
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage());
                }

                break;
            case "START":

                hideButton(View.GONE, View.GONE, View.GONE);

                tv_new_job_activity_timer.setVisibility(View.GONE);
                ll_layout_job_accept_reject.setVisibility(View.GONE);
                ll_layout_job_arrived.setVisibility(View.GONE);
                ll_layout_job_start.setVisibility(View.GONE);

                tv_new_job_address.setVisibility(View.VISIBLE);
                ll_layout_job_finish.setVisibility(View.VISIBLE);
                ll_layout_job_detial.setVisibility(View.VISIBLE);
                tv_new_job_mobile.setVisibility(View.VISIBLE);
                tv_new_job_mobile.setText(mobileNo);
                tv_new_job_address.setText(address);
                tv_new_job_activity_title.setText(title);


                try {
                    setMarkerOfCustomer(lat, lng);
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage());
                }

                break;

            case "FINISH":

                showAlertDialogForCompletedJob();

                cancelNotification(getActivity(), 0);

                break;

            case "CANCEL":

                cancellationReson();

        }
    }

    public void cancellationReson() {

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.dialog_cancellation_reason, null);

        final RadioButton rb_19 = (RadioButton) mView.findViewById(R.id.rb_19);
        final RadioButton rb_20 = (RadioButton) mView.findViewById(R.id.rb_20);
        final RadioButton rb_21 = (RadioButton) mView.findViewById(R.id.rb_21);
        final RadioButton rb_22 = (RadioButton) mView.findViewById(R.id.rb_22);

        Button btn_exit = (Button) mView.findViewById(R.id.btn_exit);
        Button btn_cancel_jobs = (Button) mView.findViewById(R.id.btn_cancel_job);

        mBuilder.setView(mView);

        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        btn_cancel_jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String radioButtonChecked = "";
                if (rb_19.isChecked()) {
                    radioButtonChecked = "19";
                } else if (rb_20.isChecked()) {
                    radioButtonChecked = "20";
                } else if (rb_21.isChecked()) {
                    radioButtonChecked = "21";
                } else if (rb_22.isChecked()) {
                    radioButtonChecked = "22";
                }

                isError = RequestUrlForNavigationDrawerActivity.JobCancel(getActivity(), jobId, radioButtonChecked);
                if (isError)
                    return;

                if (!isMyServiceRunning(MyService.class)) {
                    getActivity().startService(new Intent(getActivity(), MyService.class));
                }


                btn_start.setClickable(true);

                hideButton(View.VISIBLE, View.GONE, View.GONE);

                tv_new_job_activity_timer.setVisibility(View.GONE);
                ll_layout_job_accept_reject.setVisibility(View.GONE);

                tv_new_job_address.setVisibility(View.GONE);
                ll_layout_job_detial.setVisibility(View.GONE);
                ll_layout_job_arrived.setVisibility(View.GONE);
                ll_layout_job_start.setVisibility(View.GONE);
                tv_new_job_mobile.setVisibility(View.GONE);


                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", 0);
                preferences.edit().remove("JOBID");
                preferences.edit().remove("ADDRESS");
                preferences.edit().remove("TITLE");
                preferences.edit().remove("LAT");
                preferences.edit().remove("LNG");
                preferences.edit().remove("CUSTOMERMOBILE");
                preferences.edit().commit();

                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                dialog.cancel();
            }
        });
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

    }

    public void showAlertDialogForCancelJob() {
        @SuppressLint("RestrictedApi") AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme));
        dialogBuilder.setTitle("Alert");
        dialogBuilder.setMessage("Job Cancelled By Customer");
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                hideButton(View.VISIBLE, View.GONE, View.GONE);

                tv_new_job_activity_timer.setVisibility(View.GONE);
                ll_layout_job_accept_reject.setVisibility(View.GONE);
                ll_layout_job_start.setVisibility(View.GONE);

                btn_start.setClickable(true);


                tv_new_job_address.setVisibility(View.GONE);
                ll_layout_job_detial.setVisibility(View.GONE);
                ll_layout_job_arrived.setVisibility(View.GONE);
                tv_new_job_mobile.setVisibility(View.GONE);
                ll_layout_job_finish.setVisibility(View.GONE);


                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", 0);
                preferences.edit().remove("JOBID");
                preferences.edit().remove("ADDRESS");
                preferences.edit().remove("TITLE");
                preferences.edit().remove("LAT");
                preferences.edit().remove("LNG");
                preferences.edit().remove("CUSTOMERMOBILE");
                preferences.edit().commit();

                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                cancelNotification(getActivity(), 0);

                if (!isMyServiceRunning(MyService.class)) {
                    getActivity().startService(new Intent(getActivity(), MyService.class));
                }

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showAlertDialogForCompletedJob() {

        @SuppressLint("RestrictedApi") AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme));
        dialogBuilder.setTitle("Alert");
        dialogBuilder.setMessage("Job Completed ?");
        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //showCustomDialogForJobFinish(ConfigURL.getJobTitle(getActivity()));

                showCustomDialogForJob();

                hideButton(View.VISIBLE, View.GONE, View.GONE);

                tv_new_job_activity_timer.setVisibility(View.GONE);
                ll_layout_job_accept_reject.setVisibility(View.GONE);
                ll_layout_job_arrived.setVisibility(View.GONE);
                ll_layout_job_start.setVisibility(View.GONE);

                tv_new_job_address.setVisibility(View.GONE);
                ll_layout_job_finish.setVisibility(View.GONE);
                ll_layout_job_detial.setVisibility(View.GONE);
                tv_new_job_mobile.setVisibility(View.GONE);

                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

            }
        });
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showCustomDialogForJob() {

        Fragment fragmentName = null;
        Fragment JobFinish = new JobFinishFragment();
        fragmentName = JobFinish;
        replaceFragment(fragmentName);

    }

    /*public void showCustomDialogForJobFinish(String title) {
        String job_fare = "", job_amount = "";

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.dialog_finish_job, null);
        final TextView tv_new_job_title = (TextView) mView.findViewById(R.id.tv_new_job_title);
        tv_new_job_title.setText(title);
        final EditText et_job_finish_fare = (EditText) mView.findViewById(R.id.et_job_finish_fare);
        final EditText et_job_finish_amount = (EditText) mView.findViewById(R.id.et_job_finish_amount);
        final RatingBar rate_customer = (RatingBar) mView.findViewById(R.id.rate_customer);
        Button btn_finish_job_url = (Button) mView.findViewById(R.id.btn_finish_job_url);
        mBuilder.setView(mView);
        final TextView tv_new_job_hours = (TextView) mView.findViewById(R.id.tv_new_job_hours);
        RequestUrlForNavigationDrawerActivity.setCurrentJobTime(getActivity(), jobId, tv_new_job_hours);

        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        btn_finish_job_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_job_finish_fare.getText().toString().isEmpty()) {
                    et_job_finish_fare.setError("Fare Cann't be Empty");
                    requestFocus(et_job_finish_fare);
                    return;
                }
                if (et_job_finish_amount.getText().toString().isEmpty()) {
                    et_job_finish_amount.setError("Amount Cann't be Empty");
                    requestFocus(et_job_finish_fare);
                    return;
                }
                if (rate_customer.getRating() == 0.0) {
                    Toast.makeText(getActivity(), "Please Rate Customer Experience", Toast.LENGTH_LONG).show();
                    return;
                }
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                isError = RequestUrlForNavigationDrawerActivity.workerJobFinish(getActivity(), ConfigURL.getJobID(getActivity()), et_job_finish_fare.getText().toString(), et_job_finish_amount.getText().toString(), Float.toString(rate_customer.getRating()));
                if (isError)
                    return;

                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", 0);
                preferences.edit().remove("JOBID");
                preferences.edit().remove("ADDRESS");
                preferences.edit().remove("TITLE");
                preferences.edit().remove("LAT");
                preferences.edit().remove("LNG");
                preferences.edit().remove("CUSTOMERMOBILE");
                preferences.edit().commit();

                hideButton(View.VISIBLE, View.GONE, View.GONE);

                tv_new_job_activity_timer.setVisibility(View.GONE);
                ll_layout_job_accept_reject.setVisibility(View.GONE);
                ll_layout_job_arrived.setVisibility(View.GONE);
                ll_layout_job_start.setVisibility(View.GONE);

                tv_new_job_address.setVisibility(View.GONE);
                ll_layout_job_finish.setVisibility(View.GONE);
                ll_layout_job_detial.setVisibility(View.GONE);
                tv_new_job_mobile.setVisibility(View.GONE);

                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                dialog.cancel();
            }
        });
    }
  private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
*/
    public void acceptableAmountDialog() {

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.user_input_dialog_box, null);
        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        Button user_dialog_done = (Button) mView.findViewById(R.id.user_dialog_done);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        user_dialog_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String jobAmount = "";
                jobAmount = userInputDialogEditText.getText().toString();
                if (jobAmount.matches("")) {
                    userInputDialogEditText.setError("Amount Cannot Be Empty");
                    return;
                } else {
                    isError = RequestUrlForNavigationDrawerActivity.jobAcceptableAmount(getActivity(), ConfigURL.getJobID(getActivity()), userInputDialogEditText.getText().toString());
                    if (isError)
                        return;

                    btn_start.setClickable(false);

                    SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("CLICKABLE", "false");
                    editor.commit();


                    dialog.dismiss();
                }
            }
        });
    }

    public void hideButton(int offline, int online, int cancel) {
        btn_worker_offline.setVisibility(offline);
        btn_worker_online.setVisibility(online);
        btn_cancel_job.setVisibility(cancel);
    }

    @Override
    public void callbackCall(String workerJobId, String Status, String title, String lat, String lng, String cMobile) {
        String workAddress = "";
        if (ConfigURL.getJobID(getActivity()).length() > 0) {
            updateLayout(Status);
        } else {
            workAddress = getAddress(Double.parseDouble(lat), Double.parseDouble(lng));
            SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("JOBID", workerJobId);
            editor.putString("ADDRESS", workAddress);
            editor.putString("TITLE", "Job Title : " + title);
            editor.putString("LAT", lat);
            editor.putString("LNG", lng);
            editor.putString("CUSTOMERMOBILE", cMobile);
            editor.commit();
            updateLayout(Status);

            getActivity().startService(new Intent(getActivity(), MyService.class));

        }
    }

    public String getAddress(Double lat, Double lng) {
        String address = "";

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        address = addresses.get(0).getAddressLine(0);

        return address;
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_dashboard, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    private void clearBackStack() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 1) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(1);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void customNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(getActivity())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Labor On Demand")
                        .setContentText("You Are Online");
        builder.setOngoing(true);
        final NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(111, builder.build());
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
