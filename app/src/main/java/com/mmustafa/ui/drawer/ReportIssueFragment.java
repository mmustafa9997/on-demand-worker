package com.mmustafa.ui.drawer;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportIssueFragment extends Fragment implements View.OnClickListener {

    EditText et_input_subject, et_input_message;
    Button btn_send_report;

    public ReportIssueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_report_issue, container, false);

        getActivity().setTitle("Report Issue");

        et_input_subject = (EditText) rootView.findViewById(R.id.et_input_subject);
        et_input_message = (EditText) rootView.findViewById(R.id.et_input_message);
        btn_send_report = (Button) rootView.findViewById(R.id.btn_send_report);
        btn_send_report.setOnClickListener(this);

        return rootView;
    }

    public void composeEmail(String messege, String subject) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "mmustafasesudia@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, messege);
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send_report:
                composeEmail(et_input_message.getText().toString(), et_input_subject.getText().toString());
                break;
        }
    }
}
