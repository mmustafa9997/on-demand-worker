package com.mmustafa.ui.drawer.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Qasim Ahmed on 04/01/2018.
 */

public class WorkerTransactionHistory {
    String jobId;
    String Transfer;
    String Amount;
    String updateTime;
    String Balance;
    String Category;

    public WorkerTransactionHistory(String transfer, String amount, String updateTime, String balance, String jobID) {
        Transfer = transfer;
        Amount = amount;
        this.updateTime = updateTime;
        Balance = balance;
        jobId = jobID;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getTransfer() {
        return Transfer;
    }

    public void setTransfer(String transfer) {
        Transfer = transfer;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public WorkerTransactionHistory(JSONObject jsonObject) {
        try {
            this.jobId = jsonObject.getString("jobId");
            this.Transfer = jsonObject.getString("Transfer");
            this.Amount = jsonObject.getString("Amount");
            this.updateTime = jsonObject.getString("updateTime");
            this.Balance = jsonObject.getString("Balance");
            this.Category = jsonObject.getString("JobTitle");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
