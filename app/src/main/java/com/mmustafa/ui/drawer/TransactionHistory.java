package com.mmustafa.ui.drawer;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.Adapters.AdapterTransactionHistory;
import com.mmustafa.ui.drawer.Models.WorkerTransactionHistory;
import com.mmustafa.ui.drawer.R;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.utils.NetworkConnectivityClass;
import com.mmustafa.ui.drawer.utils.ProgressDialogClass;
import com.mmustafa.ui.drawer.utils.RequestUrlForNavigationDrawerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionHistory extends Fragment {

    TextView tv_wallet_amount_transaction;
    String walletAmountSetOnTextViewTransaction = "";
    ArrayList<WorkerTransactionHistory> data;

    public TransactionHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_transaction, container, false);

        getActivity().setTitle("Transaction History");

        tv_wallet_amount_transaction = (TextView) rootView.findViewById(R.id.tv_wallet_amount_transaction);
        if (NetworkConnectivityClass.isNetworkAvailable(getActivity())) {
            RequestUrlForNavigationDrawerActivity.getWalletAmount(getActivity(), tv_wallet_amount_transaction);
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), "Internet Not Connected",
                    Snackbar.LENGTH_SHORT).show();
        }
        final RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);




        AndroidNetworking.get(ConfigURL.URL_GET_WORKER_TRANSACTION)
                .addQueryParameter("pMobileNo", ConfigURL.getMobileNumber(getActivity()))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        ProgressDialogClass.hideProgress();

                        data = new ArrayList<>();

                        try {
                            JSONArray jsonArray = response.getJSONArray("workerWallet");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String jobId, Transfer, Amount, updateTime, Balance, Category;

                                WorkerTransactionHistory transactionHistory = new WorkerTransactionHistory(jsonArray.getJSONObject(i));
                                Transfer = transactionHistory.getTransfer();
                                Amount = transactionHistory.getAmount();
                                updateTime = transactionHistory.getUpdateTime();
                                Balance = transactionHistory.getBalance();
                                jobId = transactionHistory.getJobId();

                                WorkerTransactionHistory obj = new WorkerTransactionHistory(Transfer, Amount, updateTime, Balance, jobId);
                                data.add(obj);

                            }

                            AdapterTransactionHistory adapter = new AdapterTransactionHistory(getActivity(), data);
                            rv.setAdapter(adapter);

                        } catch (JSONException e) {
                            ProgressDialogClass.hideProgress();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
//                        ProgressDialogClass.hideProgress();
                    }
                });


//        AdapterTransactionHistory adapter = new AdapterTransactionHistory(new String[]{"Plumber", "Cleaning"}, new String[]{"PL-326", "CL-331"}, new String[]{"Rs 750", "Rs 880"});
//        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        return rootView;
    }

}
