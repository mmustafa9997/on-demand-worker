package com.mmustafa.ui.drawer.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Qasim Ahmed on 17/11/2017.
 */

public class Worker {

    int worker_id;
    String worker_name;
    String worker_email;
    String worker_mobileNo;
    String worker_password;


    public String getWorker_name() {
        return worker_name;
    }


    public void setWorker_name(String worker_name) {
        this.worker_name = worker_name;
    }

    public Worker(String worker_name) {

        this.worker_name = worker_name;
    }

    public Worker(int worker_id, String worker_name, String worker_email, String worker_mobileNo, String worker_password) {
        this.worker_id = worker_id;
        this.worker_name = worker_name;
        this.worker_email = worker_email;
        this.worker_mobileNo = worker_mobileNo;
        this.worker_password = worker_password;
    }

    public int getWorker_id() {

        return worker_id;
    }

    public void setWorker_id(int worker_id) {
        this.worker_id = worker_id;
    }

    public String getWorker_email() {
        return worker_email;
    }

    public void setWorker_email(String worker_email) {
        this.worker_email = worker_email;
    }

    public String getWorker_mobileNo() {
        return worker_mobileNo;
    }

    public void setWorker_mobileNo(String worker_mobileNo) {
        this.worker_mobileNo = worker_mobileNo;
    }

    public String getWorker_password() {
        return worker_password;
    }

    public void setWorker_password(String worker_password) {
        this.worker_password = worker_password;
    }

    public Worker(JSONObject jsonObject)
    {
        try {
            this.worker_name = jsonObject.getString("personName");
            this.worker_email = jsonObject.getString("personEmail");
            this.worker_mobileNo =jsonObject.getString("personMobileNo");
            this.worker_password = jsonObject.getString("personPassword");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
