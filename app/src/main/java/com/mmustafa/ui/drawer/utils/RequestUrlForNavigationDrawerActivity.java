package com.mmustafa.ui.drawer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.Models.WorkerJobStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class RequestUrlForNavigationDrawerActivity {

    public static final String TAG = "URL CLASS ";
    public static String msg = "";
    public static boolean error = false;
    static boolean isError = false;
    public List<MyCallback> listeners = new ArrayList<MyCallback>();

    public static void getWalletAmount(Context context, final TextView textView) {

        AndroidNetworking.get(ConfigURL.URL_GET_WALLET_AMOUNT)
                .addQueryParameter("pMobile", ConfigURL.getMobileNumber(context))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            msg = response.getString("wallet");
                            error = response.getBoolean("error");
                            Log.v(TAG + "Wallet Url", msg);

                            if ((!error) && (msg != ""))
                                textView.setText("Rs. " + msg);
                            else if (!error && msg == "")
                                textView.setText("");
                        } catch (JSONException e) {
                            Log.e(TAG + "Wallet", e.getMessage());
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "Wallet Url" + anError);
                    }
                });
    }

    public static void updateWorkerLocation(final Context context, Double mLat, Double mLng) {

        AndroidNetworking.post(ConfigURL.URL_UPDATE_WORKER_LOCATION)
                .addBodyParameter("pMobile", ConfigURL.getMobileNumber(context))
                .addBodyParameter("latitude", mLat.toString())
                .addBodyParameter("longitude", mLng.toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String msg = response.getString("message");
                            boolean error = false;
                            error = response.getBoolean("error");
                            if (!error) {
                                Log.v(TAG + "Update W Loc", msg);
                            }
                            if (error) {
                                Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e(TAG + "Update W Loc", e.getMessage());
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e(TAG, "Update Worker Location Error " + error);
                    }
                });
    }

    public static boolean sendJobAcceptance(final Context context, String jobId, String jobAcceptanceRejectId) {
        ProgressDialogClass.showProgress(context);

        AndroidNetworking.post(ConfigURL.URL_JOB_ACCEPTANCE)
                .addBodyParameter("pMobile", ConfigURL.getMobileNumber(context))
                .addBodyParameter("jobId", jobId)
                .addBodyParameter("acceptance", jobAcceptanceRejectId)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();

                        try {
                            response.getString("message");
                            if (response.getBoolean("error")) {
                                isError = true;
                                Log.v(TAG, "ACCEPT NOT DONE");
//                                Toast.makeText(context, "Job Not Accepted", Toast.LENGTH_SHORT).show();
                            } else {
                                isError = false;

                                Log.v(TAG, "ACCEPT DONE");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
//                        Toast.makeText(context, "Job Error", Toast.LENGTH_LONG).show();
                        isError = true;
                        Log.e(TAG, "Acceptance Job Error " + error);

                    }
                });
        return isError;
    }

    public static boolean workerArrived(final Context context, String jobId) {
        ProgressDialogClass.showProgress(context);

        AndroidNetworking.post(ConfigURL.URL_JOB_WORKER_ARRIVED)
                .addBodyParameter("jobId", jobId.toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();

                        try {
                            response.getString("message");
                            if (response.getBoolean("error")) {
                                isError = true;
                                Log.v(TAG, "ARRIVED NOT DONE");
//                                Toast.makeText(context, "Arrived Fail", Toast.LENGTH_SHORT).show();
                            } else {
                                isError = false;
                                Log.v(TAG, "ARRIVED DONE");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        isError = true;
                        Log.e(TAG, "Arrived Job Error " + error);

                    }
                });
        return isError;
    }

    public static boolean workerJobStart(final Context context, String jobId) {
        ProgressDialogClass.showProgress(context);

        AndroidNetworking.post(ConfigURL.URL_JOB_WORKER_START)
                .addBodyParameter("jobId", jobId.toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();

                        try {
                            response.getString("message");
                            if (response.getBoolean("error")) {
                                isError = true;
                                Log.v(TAG, "START NOT DONE");
//                                Toast.makeText(context, "Start Fail", Toast.LENGTH_SHORT).show();
                            } else {
                                isError = false;
                                Log.v(TAG, "START DONE");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        Log.e(TAG, "Start Job Error " + error);
                        isError = true;

                    }
                });

        return isError;
    }

    public static boolean workerJobFinish(final Context context, String jobId, String jobCost, String jobCashRecieved, String rateByWorker) {
        ProgressDialogClass.showProgress(context);

        AndroidNetworking.post(ConfigURL.URL_JOB_WORKER_FINISH)
                .addBodyParameter("jobId", jobId.toString())
                .addBodyParameter("jobCost", jobCost.toString())
                .addBodyParameter("jobCashRecieved", jobCashRecieved.toString())
                .addBodyParameter("rateByWorker", rateByWorker.toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();
                        try {
                            response.getString("message");
                            if (response.getBoolean("error")) {
                                isError = true;
                                Log.v(TAG, "FINISH NOT DONE");
//                                Toast.makeText(context, "Finish Fail", Toast.LENGTH_SHORT).show();
                            } else {
                                isError = false;
                                Log.v(TAG, "FINISH DONE");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        Log.e(TAG, "Finish Job Error " + error);
                        isError = true;
                    }
                });

        return isError;
    }

    public static void setWokerImage(final Context context) {

        /*if (ConfigURL.getWorkerProfilePic(context).length() > 0) {
            return;
        }*/
        AndroidNetworking.get(ConfigURL.URL_GET_WORKER_IMAGE)
                .addQueryParameter("pMobile", ConfigURL.getMobileNumber(context))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String imageURL = "";
                        try {
                            JSONObject jsonObject;
                            JSONArray jsonArray = response.getJSONArray("worker");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                jsonObject = jsonArray.getJSONObject(i);
                                imageURL = jsonObject.getString("WorkerImages");

                            }
                            SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("PROFILEPIC", imageURL);
                            editor.commit();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e(TAG, "Image Error " + error);
                    }
                });
    }

    public static void setCurrentJobTime(final Context context, String jobID, final TextView textView , final TextView et_jobCost) {

        ProgressDialogClass.showProgress(context);

        AndroidNetworking.get(ConfigURL.URL_GET_JOB_TIME)
                .addQueryParameter("jobId", jobID)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();
                        String formatTime = "" , jobCost = "";
                        try {
                            JSONObject jsonObject;
                            JSONArray jsonArray = response.getJSONArray("worker");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                jsonObject = jsonArray.getJSONObject(i);
                                formatTime = jsonObject.getString("formatedTime");
                                jobCost = jsonObject.getString("JobCost");

                            }
                            textView.setText("Work Hour : " + formatTime);
                            et_jobCost.setText("Job Cost : "+jobCost);

                            /*SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("PROFILEPIC", formatedTime);
                            editor.commit();*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        Log.e(TAG, "Job TIme Error " + error);
                    }
                });
    }

    public static boolean JobCancel(Context context, String jobID, String reasonId) {

        ProgressDialogClass.showProgress(context);
        AndroidNetworking.post(ConfigURL.URL_POST_JOB_CANCEL)
                .addBodyParameter("reasonId", reasonId)
                .addBodyParameter("jobId", jobID)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();

                        try {
                            response.getString("message");
                            if (response.getBoolean("error")) {
                                isError = true;
                                Log.v(TAG, "Job Cancel NOT DONE");
//                                Toast.makeText(context, "Finish Fail", Toast.LENGTH_SHORT).show();
                            } else {
                                isError = false;
                                Log.v(TAG, "Cancel Job DONE");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        isError = false;
                    }
                });
        return isError;
    }

    public static boolean jobAcceptableAmount(Context context, String jobID, String jobAmount) {

        ProgressDialogClass.showProgress(context);
        AndroidNetworking.post(ConfigURL.URL_ACCEPTABLE_AMOUNT)
                .addBodyParameter("jobCost", jobAmount)
                .addBodyParameter("jobId", jobID)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();

                        try {
                            response.getString("message");
                            if (response.getBoolean("error")) {
                                isError = true;
//                                Toast.makeText(context, "Finish Fail", Toast.LENGTH_SHORT).show();
                            } else {
                                isError = false;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        isError = false;
                    }
                });
        return isError;
    }

    public void addListener(MyCallback listener) {
        listeners.add(listener);
    }

    public void notifySomethingHappened(String jobId, String Status, String title, String lat, String lng, String cMobile) {
        for (MyCallback listener : listeners) {
            listener.callbackCall(jobId, Status, title, lat, lng, cMobile);
        }
    }

    public void checkWorkerJobStatus(final Context context) {

        AndroidNetworking.get(ConfigURL.URL_CHECK_JOB_STATUS)
                .addQueryParameter("workerMobileNo", ConfigURL.getMobileNumber(context))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String jobID = "", jobStatus = "", title = "", lat = "", lng = "", cMobile = "";
                        try {
                            JSONArray jsonArray = response.getJSONArray("workerJob");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                WorkerJobStatus workerJobStatus = new WorkerJobStatus(jsonArray.getJSONObject(i));
                                jobID = workerJobStatus.getJobId();
                                jobStatus = workerJobStatus.getJobStatus();
                                title = workerJobStatus.getTitle();
                                lat = workerJobStatus.getLatitude();
                                lng = workerJobStatus.getLongitude();
                                cMobile = workerJobStatus.getCustomerMobileNo();

                                notifySomethingHappened(jobID, jobStatus, title, lat, lng, cMobile);
                                // Toast.makeText(context, "" + checkJobState.get(0) + "" + checkJobState, Toast.LENGTH_LONG).show();
                                Log.v(TAG, "Check Job Error " + error);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e(TAG, "Finish Job Error " + error);
                    }
                });
    }

    public interface MyCallback {
        void callbackCall(String jobId, String Status, String title, String lat, String lng, String cMobile);
    }
}

