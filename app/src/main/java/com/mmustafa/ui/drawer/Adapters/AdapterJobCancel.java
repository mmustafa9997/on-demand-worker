package com.mmustafa.ui.drawer.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mmustafa.ui.drawer.Models.WorkerJobs;
import com.mmustafa.ui.drawer.R;

import java.util.ArrayList;


public class AdapterJobCancel extends RecyclerView.Adapter<AdapterJobCancel.MyViewHolder> {

    private Context acontext;
    private ArrayList<WorkerJobs> arrayList ;

    public  class MyViewHolder extends RecyclerView.ViewHolder{

        public CardView mCardView;
        public TextView  tv_cancel_job_category, tv_cancel_job_customer_name,tv_cancel_job_date,tv_cancel_job_bookingId;

        public MyViewHolder(View v){
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view_order_cancel);
            tv_cancel_job_category = (TextView) v.findViewById(R.id.tv_cancel_job_category);
            tv_cancel_job_customer_name = (TextView) v.findViewById(R.id.tv_cancel_job_customer_name);
            tv_cancel_job_date = (TextView) v.findViewById(R.id.tv_cancel_job_date);
            tv_cancel_job_bookingId = (TextView) v.findViewById(R.id.tv_cancel_job_bookingId);
        }

    }
    public AdapterJobCancel(Context context,ArrayList<WorkerJobs> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterJobCancel.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_order_cancel, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        WorkerJobs current = arrayList.get(position);
        holder.tv_cancel_job_category.setText(current.getJobTitle());
        holder.tv_cancel_job_customer_name.setText(current.getCutomerName());
        holder.tv_cancel_job_date.setText(current.getJobCreationTime());
        holder.tv_cancel_job_bookingId.setText(current.getJobId());
    }

    @Override
    public int getItemCount() {return arrayList.size(); }

}
