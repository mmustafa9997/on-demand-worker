package com.mmustafa.ui.drawer.TabLayout;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.Adapters.AdapterJobCompleted;
import com.mmustafa.ui.drawer.CompletedJobDetailFragment;
import com.mmustafa.ui.drawer.Models.WorkerJobs;
import com.mmustafa.ui.drawer.R;
import com.mmustafa.ui.drawer.utils.ConfigURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CompletedJobsFragment extends Fragment implements AdapterJobCompleted.ItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    ArrayList<WorkerJobs> data;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView rv;

    public CompletedJobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_one, container, false);
        rv = (RecyclerView) rootView.findViewById(R.id.rv_order_completed);
        rv.setHasFixedSize(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.contentView);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        loadData();

        return rootView;
    }

    public void loadData() {
        AndroidNetworking.get(ConfigURL.URL_GET_WORKER_ORDERS)
                .addQueryParameter("workerMobileNo", ConfigURL.getMobileNumber(getActivity()))
                .addQueryParameter("jobStatus", "COMPLETED")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        refreshItems();
                        data = new ArrayList<>();
                        Log.d("AA Response", "" + response);

                        try {
                            JSONArray jsonArray = response.getJSONArray("workerJobs");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String jobId, cutomerName, Worker_Person_personId, Customer_Person_personId, jobStatus, jobBookingStatus, jobCost, jobHours, jobCreationTime, jobCompleteTime, jobTitle, customerRating, workerRating;

                                WorkerJobs workerCompletedJobs = new WorkerJobs(jsonArray.getJSONObject(i));

                                jobId = workerCompletedJobs.getJobId();
                                Worker_Person_personId = workerCompletedJobs.getWorker_Person_personId();
                                Customer_Person_personId = workerCompletedJobs.getCustomer_Person_personId();
                                jobStatus = workerCompletedJobs.getJobStatus();
                                jobBookingStatus = workerCompletedJobs.getJobBookingStatus();
                                jobCost = workerCompletedJobs.getJobCost();
                                jobHours = workerCompletedJobs.getJobHours();
                                jobCreationTime = workerCompletedJobs.getJobCreationTime();
                                jobCompleteTime = workerCompletedJobs.getJobCompleteTime();
                                cutomerName = workerCompletedJobs.getCutomerName();
                                jobTitle = workerCompletedJobs.getJobTitle();
                                customerRating = workerCompletedJobs.getRattingByCustomer();
                                workerRating = workerCompletedJobs.getRattingByWorker();

                                Log.d("AA", "" + jobTitle + "" + cutomerName + "");

                                WorkerJobs obj = new WorkerJobs(jobId, cutomerName, Worker_Person_personId, Customer_Person_personId, jobStatus, jobBookingStatus, jobCost, jobHours, jobCreationTime, jobCompleteTime, jobTitle, customerRating, workerRating);
                                data.add(obj);

                            }

                            AdapterJobCompleted adapter = new AdapterJobCompleted(getActivity(), data);
                            rv.setAdapter(adapter);
                            adapter.setClickListener(CompletedJobsFragment.this);


                        } catch (JSONException e) {
                            refreshItems();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        refreshItems();
                    }
                });

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onClick(View view, int position) {

        Fragment fragmentName = null;
        Fragment CompletedJobDetailFragment = new CompletedJobDetailFragment();
        fragmentName = CompletedJobDetailFragment;

        Bundle args = new Bundle();

        args.putString("jobId", data.get(position).getJobId());
        args.putString("Worker_Person_personId", data.get(position).getCustomer_Person_personId());
        args.putString("Customer_Person_personId", data.get(position).getWorker_Person_personId());
        args.putString("jobStatus", data.get(position).getJobStatus());
        args.putString("jobBookingStatus", data.get(position).getJobBookingStatus());
        args.putString("jobCost", data.get(position).getJobCost());
        args.putString("jobHours", data.get(position).getJobHours());
        args.putString("jobCreationTime", data.get(position).getJobCreationTime());
        args.putString("jobCompleteTime", data.get(position).getJobCompleteTime());
        args.putString("cutomerName", data.get(position).getCutomerName());
        args.putString("jobTitle", data.get(position).getJobTitle());
        args.putString("customerRating", data.get(position).getRattingByCustomer());
        args.putString("workerRating", data.get(position).getRattingByWorker());
        fragmentName.setArguments(args);

        replaceFragment(fragmentName);
    }

    public void refreshItems() {
        onItemsLoadComplete();
    }

    public void onItemsLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_dashboard, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}