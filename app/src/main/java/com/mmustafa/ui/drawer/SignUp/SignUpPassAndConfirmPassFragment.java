package com.mmustafa.ui.drawer.SignUp;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mmustafa.ui.drawer.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpPassAndConfirmPassFragment extends Fragment implements View.OnClickListener {

    Button btn_done;
    EditText et_input_pass, et_input_confirm_pass;
    String full_name, email, phone, password, gender, category;

    byte[] byteArray;

    public SignUpPassAndConfirmPassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sign_up_pass_and_confirm_pass, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();


        Bundle bundle = getArguments();

        if (bundle != null) {
            full_name = (String) bundle.get("full_name");
            phone = (String) bundle.get("phone");
            email = (String) bundle.get("email");
            gender = (String) bundle.get("gender");
            category = (String) bundle.get("category");
            byteArray = getArguments().getByteArray("image");
        }

        btn_done = (Button) rootView.findViewById(R.id.btn_done);
        et_input_pass = (EditText) rootView.findViewById(R.id.et_input_pass);
        et_input_confirm_pass = (EditText) rootView.findViewById(R.id.et_input_confirm_pass);
        btn_done.setOnClickListener(this);

        return rootView;
    }


    private void submitForm() {
        if (!validatePassword()) {
            return;
        }
        if (!validatePasswordConfirmation()) {
            return;
        }

        Fragment fragmentName = null;
        Fragment SignUpVerification = new SignUpVerification();
        fragmentName = SignUpVerification;

        Bundle args = new Bundle();
        password = et_input_confirm_pass.getText().toString();
        args.putString("full_name", full_name);
        args.putString("email", email);
        args.putString("phone", phone);
        args.putString("r_password", password);
        args.putString("gender", gender);
        args.putString("category", category);
        args.putByteArray("image",byteArray);
        fragmentName.setArguments(args);
        replaceFragment(fragmentName);

//        Toast.makeText(getActivity(), "Category" + category + "Full Name" + full_name + "Email" + email + "Phone" + phone + "Gender" + gender+"ByteArray "+byteArray, Toast.LENGTH_LONG).show();

    }


    private boolean validatePassword() {
        if (et_input_pass.getText().toString().trim().isEmpty()) {
            et_input_pass.setError("Password Cannot Be Empty");
            requestFocus(et_input_pass);
            return false;
        }
        return true;
    }


    private boolean validatePasswordConfirmation() {
        if (et_input_confirm_pass.getText().toString().trim().isEmpty()) {
            et_input_confirm_pass.setError("Confirm Password Cannot Be Empty");
            requestFocus(et_input_confirm_pass);
            return false;
        } else if (!et_input_pass.getText().toString().equals(et_input_confirm_pass.getText().toString())) {
            et_input_confirm_pass.setError("Password Not Match");
            requestFocus(et_input_confirm_pass);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_done:
                submitForm();
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_signin, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}
