package com.mmustafa.ui.drawer.SignUp;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.mmustafa.ui.drawer.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpPictureUploadFragment extends Fragment implements View.OnClickListener {

    Button btn_next;
    ImageView img_profile_image;
    String full_name, email, phone, gender;

    Bitmap bitmap;
    ByteArrayOutputStream bStream;
    byte[] byteArray;

    public SignUpPictureUploadFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_sign_up_picture_upload, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();


        Bundle bundle = getArguments();

        if (bundle != null) {
            full_name = (String) bundle.get("full_name");
            phone = (String) bundle.get("phone");
            email = (String) bundle.get("email");
            gender = (String) bundle.get("gender");
        }

        img_profile_image = (ImageView) rootView.findViewById(R.id.img_profile_image);
        btn_next = (Button) rootView.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
        img_profile_image.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
//                submit();
                if (byteArray != null) {

                    Fragment fragmentName = null;
                    Fragment WorkerSignUpCategoryPage = new WorkerSignUpCategoryPage();
                    fragmentName = WorkerSignUpCategoryPage;

                    Bundle args = new Bundle();
                    args.putString("full_name", full_name);
                    args.putString("email", email);
                    args.putString("phone", phone);
                    args.putString("gender", gender);
                    args.putByteArray("image",byteArray);

                    fragmentName.setArguments(args);
                    replaceFragment(fragmentName);


//                    Toast.makeText(getActivity(), "Full Name" + full_name + "Email" + email + "Phone" + phone + "Gender" + gender+ "ByteArray"+byteArray, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Please Select Image", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.img_profile_image:
                startDialog();
                break;
        }
    }

    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                getActivity());
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 1);

                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(takePicture, 0);
                    }
                });
        myAlertDialog.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == getActivity().RESULT_OK) {

                    bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    bStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                    byteArray = bStream.toByteArray();
                    img_profile_image.setImageBitmap(bitmap);

                }

                break;
            case 1:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
//                    img_profile_image.setImageURI(selectedImage);

                    bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    bStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                    byteArray = bStream.toByteArray();
                    img_profile_image.setImageBitmap(bitmap);

                }
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_signin, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}
