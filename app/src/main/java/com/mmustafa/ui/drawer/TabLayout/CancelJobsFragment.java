package com.mmustafa.ui.drawer.TabLayout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.Adapters.AdapterJobCancel;
import com.mmustafa.ui.drawer.Models.WorkerJobs;
import com.mmustafa.ui.drawer.R;
import com.mmustafa.ui.drawer.utils.ConfigURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CancelJobsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    ArrayList<WorkerJobs> data;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView rv;

    public CancelJobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_three, container, false);
        rv = (RecyclerView) rootView.findViewById(R.id.rv_order_cancel);
        rv.setHasFixedSize(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.contentView);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        loadData();

        return rootView;
    }

    public void loadData() {
        AndroidNetworking.get(ConfigURL.URL_GET_WORKER_ORDERS)
                .addQueryParameter("workerMobileNo", ConfigURL.getMobileNumber(getActivity()))
                .addQueryParameter("jobStatus", "CANCELED")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        refreshItems();
                        data = new ArrayList<>();
                        Log.d("AA Response", "" + response);

                        try {
                            JSONArray jsonArray = response.getJSONArray("workerJobs");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String personName, jobTitle, jobId, date;

                                WorkerJobs customer_orders = new WorkerJobs(jsonArray.getJSONObject(i));

                                personName = customer_orders.getCutomerName();
                                jobTitle = customer_orders.getJobTitle();
                                jobId = customer_orders.getJobId();
                                date = customer_orders.getJobCreationTime();

                                Log.d("AA", "" + jobTitle + "" + personName + "");

                                WorkerJobs obj = new WorkerJobs(personName, jobTitle, jobId, date);
                                data.add(obj);

                            }

                            AdapterJobCancel adapter = new AdapterJobCancel(getActivity(), data);
                            rv.setAdapter(adapter);


                        } catch (JSONException e) {
                            refreshItems();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        refreshItems();
                    }
                });


        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    public void refreshItems() {
        onItemsLoadComplete();
    }

    public void onItemsLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
    }
}