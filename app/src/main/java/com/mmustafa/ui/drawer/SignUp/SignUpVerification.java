package com.mmustafa.ui.drawer.SignUp;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.DrawerMainActivity;
import com.mmustafa.ui.drawer.Notification.SendRegistrationTokenFCM;
import com.mmustafa.ui.drawer.R;
import com.mmustafa.ui.drawer.utils.NetworkConnectivityClass;
import com.mmustafa.ui.drawer.utils.ProgressDialogClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpVerification extends Fragment implements View.OnClickListener {

    String fname, email, password, phone, gender, category;
    TextView countDown;
    Button verify, resend;
    //SMS Authentication Using FireBase
    EditText mVerificationField;
    private FirebaseAuth mAuth;
    String mVerificationId;
    TextView textView;
    Context context;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String LOGIN = "active";

    byte[] byteArray;
    Bitmap bitmap;

    private static final String TAG = "PhoneAuthActivity";


    public SignUpVerification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_signup_password_pin, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();


        Bundle bundle = getArguments();

        if (bundle != null) {
            if (bundle != null) {
                fname = (String) bundle.get("full_name");
                phone = (String) bundle.get("phone");
                email = (String) bundle.get("email");
                password = (String) bundle.get("r_password");
                gender = (String) bundle.get("gender");
                byteArray = getArguments().getByteArray("image");
                category = (String) bundle.get("category");
            }
        }

        mVerificationField = (EditText) rootView.findViewById(R.id.input_pin_code);
        countDown = (TextView) rootView.findViewById(R.id.countDown);
        verify = (Button) rootView.findViewById(R.id.btn_verify);
        resend = (Button) rootView.findViewById(R.id.btn_resend);
//        textView = (TextView) rootView.findViewById(R.id.textViewPin);
//
//        textView.setText("To Complete Process \n Enter Pin Code \n which you recived on " + phone);


        verify.setOnClickListener(this);
        resend.setOnClickListener(this);

       /* verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }
                verifyPhoneNumberWithCode(mVerificationId, code);
            }
        });*/

        mAuth = FirebaseAuth.getInstance();

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    //Invalid Phone Number
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };
        Log.d("auth", phone);
//        Toast.makeText(this, "Phone no: " + phone,
//                Toast.LENGTH_LONG).show();
        countDown.setVisibility(View.VISIBLE);
        verify.setVisibility(View.VISIBLE);
        startPhoneNumberVerification(phone);
        Log.d(TAG, phone);

        return rootView;
    }


    //Firebase Methods

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                            if (NetworkConnectivityClass.isNetworkAvailable(getActivity())) {
                                try {
                                    sendData();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Snackbar.make(getActivity().findViewById(android.R.id.content), "Internet Not Connected",
                                        Snackbar.LENGTH_SHORT).show();
                            }
                            FirebaseAuth.getInstance().signOut();

                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                mVerificationField.setError("Invalid code.");
                            }
                        }
                    }
                });
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                getActivity(),               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

        timerStart(resend, verify);

    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                getActivity(),               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseApp.initializeApp(getActivity());
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            /*startActivity(new Intent(getActivity(), DrawerMainActivity.class));
            getActivity().finish();*/
        }
    }

    public void timerStart(final Button visible, final Button gone) {
        new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                countDown.setText("Didn't Received Code Resend in: " + String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                countDown.setVisibility(View.GONE);
                gone.setVisibility(View.GONE);
                countDown.setVisibility(View.GONE);
                visible.setVisibility(View.VISIBLE);
            }
        }.start();
    }


    public void sendData() throws IOException {
        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        ProgressDialogClass.showProgress(getActivity());

        AndroidNetworking.upload(ConfigURL.URL_REGISTRATION)
                .addMultipartParameter("pName", fname)
                .addMultipartParameter("pEmail", email)
                .addMultipartParameter("pPass", password)
                .addMultipartParameter("pMobile", phone)
                .addMultipartParameter("pGender", gender)
                .addMultipartParameter("pCategory", category)
                .addMultipartFile("photo", returnFile(bitmap))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();
                        try {
                            String msg = response.getString("message");
                            boolean error = false;

                            error = response.getBoolean("error");
                            String file = response.getString("file");

                            if (error)
                                Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();

                            if (!error) {
                                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("LOGIN", LOGIN);
                                editor.putString("PHONE", phone);
                                editor.putString("NAME", fname);
                                editor.putString("EMAIL", email);
                                editor.commit();

                                SendRegistrationTokenFCM.sendRegistrationToServer(getActivity(), FirebaseInstanceId.getInstance().getToken());

                                Intent intent = new Intent(getActivity(), DrawerMainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                getActivity().finish();
//                                Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();

//                                Toast.makeText(SMSAuthentication.this, "" + file + "Full Name" + fname + "Email" + email + "Phone" + phone + "Gender" + gender, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        Toast.makeText(getActivity(), "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }


    public File returnFile(Bitmap bmp) throws IOException {
        File f = new File(getActivity().getCacheDir(), phone + ".png");
        f.createNewFile();
        //Convert bitmap to byte array
        Bitmap bitmap = bmp;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_verify:
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }
                verifyPhoneNumberWithCode(mVerificationId, code);

                break;
            case R.id.btn_resend:
                countDown.setVisibility(View.VISIBLE);
                verify.setVisibility(View.VISIBLE);
                resend.setVisibility(View.GONE);
                timerStart(resend, verify);
                resendVerificationCode(phone, mResendToken);
                break;
        }
    }
}
