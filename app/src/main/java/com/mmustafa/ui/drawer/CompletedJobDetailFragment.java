package com.mmustafa.ui.drawer;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedJobDetailFragment extends Fragment {

    Button btn_done;
    String jobId, cutomerName, Worker_Person_personId, Customer_Person_personId, jobStatus, jobBookingStatus, jobCost, jobHours, jobCreationTime, jobCompleteTime, jobTitle, customerRating, workerRating;
    TextView tv_job_id, tv_job_category, tv_customer_name, tv_job_hours, tv_job_cost, tv_job_status, tv_job_created_at, tv_job_completed_at;
    RatingBar ratingbar_by_partner, ratingbar_by_customer;

    public CompletedJobDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_completed_job_detail, container, false);

        Bundle bundle = getArguments();

        if (bundle != null) {
            jobId = (String) bundle.get("jobId");
            Worker_Person_personId = (String) bundle.get("Worker_Person_personId");
            Customer_Person_personId = (String) bundle.get("Customer_Person_personId");
            jobStatus = (String) bundle.get("jobStatus");
            jobBookingStatus = (String) bundle.get("jobBookingStatus");
            jobCost = (String) bundle.get("jobCost");
            jobHours = (String) bundle.get("jobHours");
            jobCreationTime = (String) bundle.get("jobCreationTime");
            jobCompleteTime = (String) bundle.get("jobCompleteTime");
            cutomerName = (String) bundle.get("cutomerName");
            jobTitle = (String) bundle.get("jobTitle");
            customerRating = (String) bundle.get("customerRating");
            workerRating = (String) bundle.get("workerRating");
        }

        tv_customer_name = (TextView) rootView.findViewById(R.id.tv_customer_name);
        tv_job_category = (TextView) rootView.findViewById(R.id.tv_job_category);
        tv_job_status = (TextView) rootView.findViewById(R.id.tv_job_status);
        tv_job_hours = (TextView) rootView.findViewById(R.id.tv_work_hours);
        tv_job_cost = (TextView) rootView.findViewById(R.id.tv_work_cost);
        tv_job_completed_at = (TextView) rootView.findViewById(R.id.tv_job_completed_at);
        tv_job_created_at = (TextView) rootView.findViewById(R.id.tv_job_created_at);
        ratingbar_by_customer = (RatingBar) rootView.findViewById(R.id.ratingbar_by_customer);
        ratingbar_by_partner = (RatingBar) rootView.findViewById(R.id.ratingbar_by_partner);


        tv_customer_name.setText(cutomerName);
        tv_job_category.setText(jobTitle);
        tv_job_status.setText(jobStatus);
        tv_job_hours.setText(jobHours);
        tv_job_cost.setText(jobCost);
        tv_job_completed_at.setText(jobCompleteTime);
        tv_job_created_at.setText(jobCreationTime);


        float customer, worker;
        try {
            customer = Float.parseFloat(customerRating);
            worker = Float.parseFloat(workerRating);
            Log.v("Rating", customer + "" + worker);
            ratingbar_by_partner.setRating(worker);
            ratingbar_by_customer.setRating(customer);

        } catch (NumberFormatException e) {

        }

        return rootView;
    }

}
