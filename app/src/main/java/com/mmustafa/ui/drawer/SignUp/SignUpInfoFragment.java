package com.mmustafa.ui.drawer.SignUp;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mmustafa.ui.drawer.utils.ConfigURL;
import com.mmustafa.ui.drawer.R;
import com.mmustafa.ui.drawer.SignIn.SignInFragment;
import com.mmustafa.ui.drawer.utils.NetworkConnectivityClass;
import com.mmustafa.ui.drawer.utils.ProgressDialogClass;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpInfoFragment extends Fragment implements View.OnClickListener {

    TextView tv_sign_in;
    Button btn_next;
    String phone;
    EditText et_input_full_name, et_input_email, et_input_phone;
    RadioButton rb_input_gender_male, rb_input_gender_female;

    public SignUpInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sign_up_info, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();


        et_input_full_name = (EditText) rootView.findViewById(R.id.et_input_full_name);
        et_input_email = (EditText) rootView.findViewById(R.id.et_input_email);
        et_input_phone = (EditText) rootView.findViewById(R.id.et_input_phone);
        rb_input_gender_male = (RadioButton) rootView.findViewById(R.id.rb_input_gender_male);
        rb_input_gender_female = (RadioButton) rootView.findViewById(R.id.rb_input_gender_female);


        tv_sign_in = (TextView) rootView.findViewById(R.id.tv_sign_in);
        btn_next = (Button) rootView.findViewById(R.id.btn_next);

        tv_sign_in.setOnClickListener(this);
        btn_next.setOnClickListener(this);

        return rootView;
    }

    public void submit() {

        if (et_input_full_name.getText().toString().isEmpty()) {
            et_input_full_name.setError("Full Name Cannot Be Empty");
            requestFocus(et_input_full_name);
            return;
        }
        if (et_input_full_name.getText().toString().startsWith(" ")) {
            et_input_full_name.setError("Space In Start And End Is Not Allowed");
            requestFocus(et_input_full_name);
            return;
        }

        if (et_input_email.getText().toString().isEmpty() || !isValidEmail(et_input_email.getText().toString())) {
            et_input_email.setError("Invalid Email");
            requestFocus(et_input_email);
            return;
        }
        if (!validatePhone()) {
            return;
        }
        if (!rb_input_gender_male.isChecked() && !rb_input_gender_male.isChecked()) {
            Toast.makeText(getActivity(), "Select Gender", Toast.LENGTH_SHORT);
            return;
        }
        if (NetworkConnectivityClass.isNetworkAvailable(getActivity())) {
            checkIfNumberExist();
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), "Internet Not Connected",
                    Snackbar.LENGTH_SHORT).show();
        }
    }


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validatePhone() {
        if (!et_input_phone.getText().toString().matches("[0][3][0-9]{9}|[3][0-9]{9}")) {
            et_input_phone.setError("Invalid Phone Number");
            requestFocus(et_input_phone);
            return false;
        } else if (et_input_phone.getText().toString().length() < 10) {
            et_input_phone.setError("Invalid Length");
            requestFocus(et_input_phone);
            return false;
        } else if (et_input_phone.getText().toString().trim().isEmpty()) {
            et_input_phone.setError("Feild Cannot Be Empty");
            requestFocus(et_input_phone);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void checkIfNumberExist() {
        ProgressDialogClass.showProgress(getActivity());

        phone = et_input_phone.getText().toString();
        if (phone.length() == 10) {
            phone = "+92" + phone;
        } else {
            phone = "+92" + phone.substring(1);
        }

        AndroidNetworking.post(ConfigURL.URL_REGISTRATION_IS_WORKER_EXIST)
                .addBodyParameter("pMobile", phone)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();
                        try {
                            String msg = response.getString("message");
                            boolean error = false;

                            error = response.getBoolean("error");

                            if (error) {
                                et_input_phone.setError(msg);
                            }

                            if (!error) {

                                Fragment fragmentName = null;
                                Fragment SignUpPictureUploadFragment = new SignUpPictureUploadFragment();
                                fragmentName = SignUpPictureUploadFragment;

                                Bundle args = new Bundle();
                                String fullname = et_input_full_name.getText().toString();
                                String email = et_input_email.getText().toString();
                                String gender;
                                if (rb_input_gender_male.isChecked()) {
                                    gender = "Male";
                                } else {
                                    gender = "Female";
                                }
                                args.putString("full_name", fullname);
                                args.putString("email", email);
                                args.putString("phone", phone);
                                args.putString("gender", gender);
                                fragmentName.setArguments(args);
                                replaceFragment(fragmentName);

//                                Toast.makeText(getActivity(), "Full Name" + fullname + "Email" + email + "Phone" + phone + "Gender" + gender, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                        Toast.makeText(getActivity(), "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_sign_in:
                Fragment fragmentName = null;
                Fragment SignInFragment = new SignInFragment();
                fragmentName = SignInFragment;
                replaceFragment(fragmentName);
                break;
            case R.id.btn_next:
                submit();
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_signin, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}
